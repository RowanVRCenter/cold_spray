﻿using UnityEngine;
using System.Collections;

public abstract class GetReal3DDevice : MonoBehaviour{

	public abstract Ray getRay();

}
