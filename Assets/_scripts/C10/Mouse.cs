﻿using UnityEngine;
using System.Collections;

public class Mouse : GetReal3DDevice{
	public static string Tag_Button_1 = "Mouse_Button1";
	public static string Tag_Button_2 = "Mouse_Button2";
	public static string Tag_Button_3 = "Mouse_Button3";
	
	
	public override Ray getRay()
	{
		return Camera.main.ScreenPointToRay(Input.mousePosition);
	}
	
}
