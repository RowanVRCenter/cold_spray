﻿using UnityEngine;
using System.Collections;

public class Information : MonoBehaviour {

	protected GameObject text;

	public string informationText = "Information";
	public GameObject textPrefab;

	public Color activeColor = new Color( 0.3f, 0.9f, 0.5f, 1.0f);
	public Color deactiveColor = new Color( 1.0f, 1.0f, 1.0f, 1.0f);

	private GameObject root;

	// Use this for initialization
	void Start () {
		root = new GameObject("InfoRoot");

		root.transform.parent = transform;
		root.transform.localPosition = new Vector3 (0, 0, 0) + Vector3.up * 0.1f;

		text = Instantiate(textPrefab, new Vector3(0,0,0), new Quaternion()) as GameObject;
		text.transform.parent = root.transform;
		text.transform.localPosition = new Vector3 (0, 0, 0);
		text.GetComponent<TextMesh>().text = informationText;

		root.SetActive(false);

	}

	public void Update()
	{
		text.transform.LookAt(2 * transform.position -  Camera.main.transform.position);
	}


	public void SetActive(bool state)
	{
		root.SetActive (state);

		if (state == true)
		{
			foreach(Renderer renderer in GetComponentsInChildren<Renderer>())
			{
				renderer.material.color = activeColor;
			}

		}
		else
		{
			foreach(Renderer renderer in GetComponentsInChildren<Renderer>())
			{
				renderer.material.color = deactiveColor;
			}
		}
	}
}
