﻿using UnityEngine;
using System.Collections;

public class InformationManager : MonoBehaviour {


	private Information previousInfoObject = null;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void NoHit()
	{
		if(previousInfoObject)
		{
			previousInfoObject.SetActive(false);
			previousInfoObject = null;
		}
	}

	//Handles checking an object and displaying its information to the screen.

	public void CheckRay( RaycastHit hit)
	{

		Information info = hit.collider.gameObject.GetComponent<Information> ();
		
		if(info == previousInfoObject)
		{
			return;
		}
		
		else if( info == null)
		{
			if( previousInfoObject )
			{
				previousInfoObject.SetActive(false);
				previousInfoObject = null;
			}
		}
		
		else
		{
			if(previousInfoObject)
				previousInfoObject.SetActive(false);
			info.SetActive(true);
			previousInfoObject = info;
		}

	}

}
