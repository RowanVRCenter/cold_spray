﻿using UnityEngine;
using System.Collections;

public class Interactable_CabinetDoor : Interactable {

	private bool doorsOpen = false;

	public float openAngle = -100.0f;
	public float doorOpenDuration = 3.0f;
	

	private GameObject[] doors;

	private Quaternion openRightDoorQuat;
	private Quaternion openLeftDoorQuat;

	private Quaternion closeDoorQuat;
	

	private AudioSource audioSource;
	
	public enum State{
		doorsOpening,
		doorsClosing,
		none
	};

	private Interactable_CabinetShelf interactable_CabinetShelf;

	private State state;
	
	// Use this for initialization
	void Start ()
	{
		doors = GameObject.FindGameObjectsWithTag("Cabinet_Door");
		interactable_CabinetShelf = GameObject.Find("FlammableCabinet_Shelf").GetComponent<Interactable_CabinetShelf>();

		audioSource = GetComponent<AudioSource>();

		openRightDoorQuat = Quaternion.Euler (0f, -openAngle, 0f);
		openLeftDoorQuat = Quaternion.Euler (0f, openAngle, 0f);
		
		closeDoorQuat = Quaternion.Euler (0f, 0f, 0f);

		state = State.none;
	}

	public override void OnInteraction( RaycastHit hit )
	{
		//Only accept an action change if we are not currently performing an action
		if(state == State.none && interactable_CabinetShelf.ShelfState == Interactable_CabinetShelf.State.none)
		{

			switch( hit.collider.gameObject.tag)
			{
			case "Cabinet_Door":
				if(!doorsOpen)
				{
					StartCoroutine("MoveDoorsOpen");
				}
				else{
					if(!interactable_CabinetShelf.ShelfOut)
					{
						StartCoroutine("MoveDoorsClose");
					}
				}
				break;
			}
		}
	}

	public void StartDoorsOpen()
	{
		StartCoroutine("MoveDoorsOpen");
	}
	
	public void StartDoorsClose()
	{
		StartCoroutine("MoveDoorsClose");
	}

	IEnumerator MoveDoorsOpen(){

		audioSource.Play();

		float currentTime = 0.0f;
		state = State.doorsOpening;

		while( currentTime <= doorOpenDuration)
		{
			doors[0].transform.localRotation = Quaternion.Slerp(closeDoorQuat, openRightDoorQuat, currentTime/doorOpenDuration);
			doors[1].transform.localRotation = Quaternion.Slerp(closeDoorQuat, openLeftDoorQuat, currentTime/doorOpenDuration);
			currentTime += Time.deltaTime;
			yield return null;
		}

		doorsOpen = true;
		state = State.none;

		interactable_CabinetShelf.StartShelfOut();
	}

	IEnumerator MoveDoorsClose()
	{

		audioSource.Play();

		float currentTime = 0.0f;

		state = State.doorsClosing;
		
		while( currentTime <= doorOpenDuration )
		{
			doors[0].transform.localRotation = Quaternion.Slerp(openRightDoorQuat, closeDoorQuat, currentTime/doorOpenDuration);
			doors[1].transform.localRotation = Quaternion.Slerp(openLeftDoorQuat, closeDoorQuat, currentTime/doorOpenDuration);
			currentTime += Time.deltaTime;
			yield return null;
		}

		doorsOpen = false;
		state = State.none;
	}

	public bool DoorsOpen {
		get {
			return this.doorsOpen;
		}
	}

	public State DoorState {
			get {
				return this.state;
			}
		}
	

}
