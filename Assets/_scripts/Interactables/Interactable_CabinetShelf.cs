﻿using UnityEngine;
using System.Collections;

public class Interactable_CabinetShelf : Interactable {

	private bool shelfOut = false;

	public float shelfOpenDuration = 2.0f;
	public float shelfDistance = 0.70f;

	private GameObject shelf;

	private Vector3 shelfOpenPosition;
	private Vector3 shelfClosePosition;
	
	public enum State{
		shelfOpening,
		shelfClosing,
		none
	};

	private Interactable_CabinetDoor interactable_cabinetDoor;

	private State state;
	
	// Use this for initialization
	void Start ()
	{

		interactable_cabinetDoor = GameObject.Find("Door_L").GetComponent<Interactable_CabinetDoor>();

		shelf = GameObject.FindGameObjectWithTag("Cabinet_Shelf");

		state = State.none;

		shelfOpenPosition = shelf.transform.localPosition + Vector3.forward * shelfDistance;
		shelfClosePosition = shelf.transform.localPosition;
	}

	public override void OnInteraction( RaycastHit hit )
	{
		//Only accept an action change if we are not currently performing an action
		if(state == State.none && interactable_cabinetDoor.DoorState == Interactable_CabinetDoor.State.none)
		{

			switch( hit.collider.gameObject.tag)
			{

			case "Cabinet_Shelf":
				if(!shelfOut && interactable_cabinetDoor.DoorsOpen)
				{
					StartShelfOut();
				}
				else{
					StartShelfIn();
				}
				break;
			}
		}
	}

	public void StartShelfOut()
	{
		StartCoroutine("MoveShelfOut");
	}

	public void StartShelfIn()
	{
		StartCoroutine("MoveShelfIn");
	}

	IEnumerator MoveShelfOut()
	{
		state = State.shelfOpening;

		float currentTime = 0.0f;

		while(  currentTime < shelfOpenDuration )
		{
			shelf.transform.localPosition = Vector3.Lerp(shelfClosePosition, shelfOpenPosition, currentTime / shelfOpenDuration );
			currentTime += Time.deltaTime;
			yield return null;
		}

		shelf.transform.localPosition = shelfOpenPosition;

		shelfOut = true;
		state = State.none;
	}

	IEnumerator MoveShelfIn()
	{
		state= State.shelfClosing;

		float currentTime = 0.0f;
		
		while(  currentTime < shelfOpenDuration )
		{
			shelf.transform.localPosition = Vector3.Lerp(shelfOpenPosition, shelfClosePosition, currentTime / shelfOpenDuration );
			currentTime += Time.deltaTime;
			yield return null;
		}

		shelf.transform.localPosition = shelfClosePosition;

		shelfOut = false;
		state = State.none;

		interactable_cabinetDoor.StartDoorsClose();

	}

	public bool ShelfOut {
		get {
			return this.shelfOut;
		}
	}

	public State ShelfState {
		get {
			return this.state;
		}
	}

}
