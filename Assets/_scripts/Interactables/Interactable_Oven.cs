﻿using UnityEngine;
using System.Collections;

public class Interactable_Oven : Interactable {

	private bool doorOpen = false;

	public float doorOpenAngle = 120.0f;
	public float doorOpenDuration = 2.5f;

	public float latchOpenAngle = 50.0f;
	public float latchOpenDuration = 0.25f;

	private GameObject door;
	private GameObject latch;

	private Quaternion doorOpenQuat;
	private Quaternion doorCloseQuat;

	private Quaternion latchOpenQuat;
	private Quaternion latchCloseQuat;

	private AudioSource audioSource;
	
	private enum State{
		doorOpening,
		doorClosing,
		latchOpening,
		latchClosing,
		none
	};

	private State state;
	
	// Use this for initialization
	void Start ()
	{
		door = GameObject.FindGameObjectWithTag("Oven_Door");
		latch = GameObject.FindGameObjectWithTag("Oven_Latch");

		audioSource = GetComponent<AudioSource>();

		doorOpenQuat = Quaternion.Euler (0f, -doorOpenAngle, 0f);
		doorCloseQuat = Quaternion.Euler (0f, 0f, 0f);

		latchOpenQuat = Quaternion.Euler (0f, latchOpenAngle, 0f);
		latchCloseQuat = Quaternion.Euler (0f, 0f, 0f);

		state = State.none;
	}

	public override void OnInteraction( RaycastHit hit )
	{
		Debug.Log (hit.collider.gameObject.tag);

		//Only accept an action change if we are not currently performing an action
		if(state == State.none)
		{

			switch( hit.collider.gameObject.tag)
			{
			case "Oven_Latch":
			case "Oven_Door":
				if(!doorOpen)
				{
					StartCoroutine("LatchCycle");
				}
				else{
					StartCoroutine("DoorClose");
				}
				break;
			}
		}
	}

	/// <summary>
	/// Open the latch and close the latch in succession.
	/// </summary>
	/// <returns>The cycle.</returns>
	IEnumerator LatchCycle()
	{
		audioSource.Play ();
		state = State.latchOpening;
		float currentTime = 0.0f;

		while(  currentTime < latchOpenDuration )
		{
			latch.transform.localRotation = Quaternion.Slerp( latchCloseQuat, latchOpenQuat, currentTime / latchOpenDuration );
			currentTime += Time.deltaTime;
			yield return null;
		}

		latch.transform.localRotation = latchOpenQuat;
		state = State.latchClosing;
		currentTime = 0.0f;

		while( currentTime < latchOpenDuration )
		{
			latch.transform.localRotation = Quaternion.Slerp( latchOpenQuat, latchCloseQuat, currentTime / latchOpenDuration);
			currentTime += Time.deltaTime;
			yield return null;
		}

		latch.transform.localRotation = latchCloseQuat;

		state = State.none;

		StartCoroutine("DoorOpen");

	}

	IEnumerator DoorOpen()
	{
		state = State.doorOpening;

		float currentTime = 0.0f;

		while( currentTime  < doorOpenDuration)
		{
			door.transform.localRotation = Quaternion.Lerp(doorCloseQuat, doorOpenQuat, currentTime / doorOpenDuration);
			currentTime += Time.deltaTime;
			yield return null;
		}

		door.transform.localRotation = doorOpenQuat;

		doorOpen = true;
		state = State.none;
	}

	IEnumerator DoorClose()
	{
		state = State.doorClosing;

		float currentTime = 0.0f;
		
		while( currentTime  < doorOpenDuration)
		{
			door.transform.localRotation = Quaternion.Lerp(doorOpenQuat, doorCloseQuat, currentTime / doorOpenDuration);
			currentTime += Time.deltaTime;
			yield return null;
		}

		door.transform.localRotation = doorCloseQuat;

		doorOpen = false;
		state = State.none;

	}
}
