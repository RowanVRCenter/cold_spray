﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Interactable_PowderFeeder : Interactable {

	protected  Dictionary< string, bool>  bolt2State;
	protected  Dictionary< string, bool>  bolt2TransitionState;

	protected bool isORingUp;
	protected bool isCapUp;
	protected bool isPowderScaled;
	protected bool isDirtClean;

	public float lerpTime = 2f;
	public float distanceToMove = 0.5f;
	public int boltsMoved;
	private GameObject hitBolt, lid, oring, powder, residualPowder;
	private bool isLerping, isScaling, isAction;

	public Material aluminumPowder;
	public Material copperPowder;
	public Material tantalumPowder;
	public Material titaniumPowder;

	private GameObject pourPowderIcon;

	private InventoryManager inventoryManager;

	// Use this for initialization
	void Start () {
	
		isAction = false;
		
		lid = GameObject.Find ("PowderFeeder/PowderFeederCap");
		oring = GameObject.Find ("PowderFeeder/ORing");
		powder = GameObject.Find ("PowderFeeder/Powder");
		residualPowder = GameObject.Find ("PowderResidualDust");

		bolt2State = new Dictionary< string, bool>();
		bolt2TransitionState = new Dictionary<string, bool>();

		for(int i = 0; i < 12 ; i++)
		{
			GameObject temp;
			if (i <= 9)
				temp = GameObject.Find("PowderCapScrew_0" + i.ToString() );
			else
				temp = GameObject.Find("PowderCapScrew_" + i.ToString() );
			bolt2State.Add (temp.name, false);
			bolt2TransitionState.Add(temp.name, false);
		}

		pourPowderIcon = this.gameObject.transform.FindChild("Silhouette_PourPowder").gameObject;
		pourPowderIcon.SetActive(false);

		inventoryManager = GameObject.FindObjectOfType<InventoryManager>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void OnInteraction (RaycastHit hit)
	{


		if (hit.collider.transform.gameObject.tag == "bolt" && bolt2TransitionState[hit.collider.name] != true) {

			bolt2TransitionState[hit.collider.gameObject.name] = true;

			checkBolt (hit.collider.gameObject);
		}
		if (hit.collider.transform.gameObject.name == "PowderFeederCap") {
			if (boltsMoved == 12) {
				if (isCapUp == false) {
					StartCoroutine(UpLerping (hit.collider.gameObject, 0f));
				} else {
					StartCoroutine(DownLerping (hit.collider.gameObject, 0f));
				}
				isCapUp = !isCapUp;
				
			} else {
				print ("Bolts are in");
			}
		}
		if (hit.collider.transform.gameObject.name == "ORing") {
			if (isCapUp == true && isORingUp == false) {
				StartCoroutine(UpLerping (hit.collider.gameObject, 0.2f));
			} else {
				if(isDirtClean)
					StartCoroutine(DownLerping (hit.collider.gameObject, 0.2f));
			}
			isORingUp = !isORingUp;
			
		}
		if (hit.collider.gameObject.name == "Powder") {
			if (isORingUp == true) {
				StartCoroutine(DownScale (hit.collider.gameObject));
				isPowderScaled = true;
			} else {
				print ("O-Ring is still in");
			}
		}
		if (hit.collider.gameObject.name == "PowderResidualDust") {
			if (isPowderScaled == true) {
				powder.SetActive (false);
				StartCoroutine(ResidualPowderFadeOut());
				isDirtClean = true;

			} else {
				print ("Powder has not been removed yet");
			}
		}

		if(hit.collider.gameObject.name == "Silhouette_PourPowder")
		{
			if(isDirtClean && inventoryManager.IsCarryingPowder)
			{
				pourPowderIcon.SetActive (false);
				powder.SetActive(true);
				UpdatePowderTexture();
				StartCoroutine(UpScale( powder));
				StartCoroutine(ResidualPowderFadeIn());
			}
		}
	}

	protected void UpdatePowderTexture()
	{
		Debug.Log (inventoryManager.Powder.gameObject.name);
		switch(inventoryManager.Powder.gameObject.name)
		{
		case "Bottle_Powder_Al(Clone)":
			powder.gameObject.GetComponent<Renderer>().material = aluminumPowder;
			break;
		case "Bottle_Powder_Cu(Clone)":
			powder.gameObject.GetComponent<Renderer>().material = copperPowder;
			break;
		case "Bottle_Powder_Ta(Clone)":
			powder.gameObject.GetComponent<Renderer>().material = tantalumPowder;
			break;
		case "Bottle_Powder_Ti(Clone)":
			powder.gameObject.GetComponent<Renderer>().material = titaniumPowder;
			break;
		}
	}

	protected IEnumerator UpLerping (GameObject hit, float zOffset)
	{
		isAction = true;
		
		hitBolt = hit;
		Vector3 startPos = hit.transform.position;
		Vector3 endPos = hit.transform.position + (hit.transform.forward * distanceToMove) - new Vector3 (0f, 0f, zOffset);
		isLerping = true;

		float currentTime = 0.0f;

		while( currentTime <= lerpTime)
		{
			hit.transform.position = Vector3.Lerp (startPos, endPos, currentTime / lerpTime);
			currentTime += Time.deltaTime;
			yield return null;
		}

		if (hit.tag == "bolt")
		{
			boltsMoved++;
			bolt2TransitionState[hit.name] = false;
		}
		isAction = false;
	}
	
	protected IEnumerator DownLerping (GameObject hit, float zOffset)
	{
		isAction = true;
		
		hitBolt = hit;
		isLerping = true;

		float currentTime = 0;
		Vector3 startPos = hit.transform.position;
		Vector3 endPos = hit.transform.position - (hit.transform.forward * distanceToMove) + new Vector3 (0f, 0f, zOffset);

		while( currentTime <= lerpTime)
		{
			hit.transform.position = Vector3.Lerp (startPos, endPos, currentTime / lerpTime);
			currentTime += Time.deltaTime;
			yield return null;
		}

		if (hit.tag == "bolt")
		{
			boltsMoved--;
			bolt2TransitionState[hit.name] = false;
		}

		isAction = false;
	}
	
	protected IEnumerator DownScale (GameObject hit)
	{
		hitBolt = hit;
		Vector3 startScale = hit.transform.localScale;
		Vector3 endScale = new Vector3 (1.0f, 1.0f, 0.0f);
		isScaling = true;

		float currentTime = 0;

		while( currentTime <= lerpTime)
		{
			hit.transform.localScale = Vector3.Lerp (startScale, endScale, currentTime / lerpTime);
			currentTime += Time.deltaTime;
			yield return null;
		}

		isAction = false;
	}

	protected IEnumerator UpScale (GameObject hit)
	{
		hitBolt = hit;
		Vector3 startScale = hit.transform.localScale;
		Vector3 endScale = new Vector3 (1.0f, 1.0f, 1.0f);
		isScaling = true;
		
		float currentTime = 0;
		
		while( currentTime <= lerpTime)
		{
			hit.transform.localScale = Vector3.Lerp (startScale, endScale, currentTime / lerpTime);
			currentTime += Time.deltaTime;
			yield return null;
		}
		
		isAction = false;
	}

	protected IEnumerator ResidualPowderFadeOut()
	{
		float currentTime = 0.0f;

		Color start = residualPowder.GetComponent<Renderer>().material.color;
		Color end = new Color(1.0f, 1.0f, 1.0f, 0.0f);

		while ( currentTime < lerpTime)
		{
			residualPowder.GetComponent<Renderer>().material.color = Color.Lerp( start, end, currentTime / lerpTime);
			currentTime += Time.deltaTime;
			yield return null;
		}
		pourPowderIcon.SetActive(true);
		residualPowder.SetActive(false);
	}

	protected IEnumerator ResidualPowderFadeIn()
	{
		float currentTime = 0.0f;
		
		Color start = residualPowder.GetComponent<Renderer>().material.color;
		Color end = new Color(1.0f, 1.0f, 1.0f, 1.0f);
		residualPowder.SetActive(true);	
		while ( currentTime < lerpTime)
		{
			residualPowder.GetComponent<Renderer>().material.color = Color.Lerp( start, end, currentTime / lerpTime);
			currentTime += Time.deltaTime;
			yield return null;
		}
		pourPowderIcon.SetActive(false);

	}

	protected void checkBolt (GameObject hit)
	{
		if (bolt2State[hit.name] == false)
		{
			Debug.Log ("Start Uplerping bolt");
			StartCoroutine( UpLerping (hit, 0f));
		}
		else
		{
			Debug.Log ("Start Downlerping bolt");
			StartCoroutine( DownLerping (hit, 0f));
		}
		bolt2State[hit.name] = !bolt2State[hit.name];
	}

}
