﻿using UnityEngine;
using System.Collections;

public class Interactable_PowerBox : Interactable {

	public float armAngle = -55;
	public float armDuration = 1f;

	private GameObject arm;
	private AudioSource audio;

	private bool isArmDown = true;

	private enum State{
		armRaising,
		armLowering,
		none,
	};


	private Quaternion armUpQuat;
	private Quaternion armDownQuat;

	State state;

	// Use this for initialization
	public void Start () {
		arm = GameObject.FindGameObjectWithTag("Powerbox_Arm");
		state = State.none;

		armUpQuat = Quaternion.Euler( armAngle, 0.0f, 0.0f);
		armDownQuat = arm.transform.localRotation;

		audio = GetComponent<AudioSource>();

	}
	
	// Update is called once per frame
	public void Update () {
	
	}

	public override void OnInteraction( RaycastHit hit )
	{
		//Only accept an action change if we are not currently performing an action
		if(state == State.none)
		{
			
			switch( hit.collider.gameObject.tag)
			{
			case "Powerbox":
			case "Powerbox_Arm":
				if(isArmDown)
				{
					StartCoroutine("ArmUp");
				}
				else{
					StartCoroutine("ArmDown");
				}
				break;

			}
		}
	}

	protected IEnumerator ArmUp()
	{
		state = State.armRaising;
		audio.Play();

		float currentTime = 0.0f;

		while( currentTime < armDuration)
		{
			arm.transform.localRotation = Quaternion.Lerp( armDownQuat, armUpQuat, currentTime / armDuration);
			currentTime += Time.deltaTime;
			yield return null;
		}
		isArmDown = false;
		state = State.none;

	}

	protected IEnumerator ArmDown()
	{
		state = State.armLowering;
		audio.Play ();
		float currentTime = 0.0f;
		
		while( currentTime < armDuration)
		{
			arm.transform.localRotation = Quaternion.Lerp( armUpQuat, armDownQuat, currentTime / armDuration);
			currentTime += Time.deltaTime;
			yield return null;
		}
		isArmDown = true;
		state = State.none;
	}

}
