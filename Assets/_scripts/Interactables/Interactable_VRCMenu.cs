﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This script should be attached to a Frame that surrounds the GUI menu
/// </summary>

public class Interactable_VRCMenu : Interactable {

	//References to all the menus
	public GameObject VRC_StartupScreen;
	public GameObject VRC_Auto;
	public GameObject VRC_Manual;
	public GameObject VRC_ExtManual;
	public GameObject VRC_IOStatus;
	public GameObject VRC_MachineParameters;
	public GameObject VRC_OperatorSettings;
	public GameObject VRC_SystemPreheat;

	protected GameObject previousMenu;
	protected GameObject currentMenu;

	public Vector3 offset;

	// Use this for initialization
	void Start () {
		VRC_StartupScreen = (GameObject)Instantiate(VRC_StartupScreen);
		VRC_Auto = (GameObject)Instantiate(VRC_Auto);
		VRC_Manual = (GameObject)Instantiate(VRC_Manual);
		VRC_ExtManual = (GameObject)Instantiate(VRC_ExtManual);
		VRC_IOStatus = (GameObject)Instantiate(VRC_IOStatus);
		VRC_MachineParameters = (GameObject)Instantiate(VRC_MachineParameters);
		VRC_OperatorSettings = (GameObject)Instantiate(VRC_OperatorSettings);
		VRC_SystemPreheat = (GameObject)Instantiate(VRC_SystemPreheat);
	
		foreach( InteractablePassThrough pt in VRC_StartupScreen.GetComponentsInChildren<InteractablePassThrough>() )
		{
			InteractablePassThrough temp = (InteractablePassThrough) pt;
			temp.interactable = this;
		}

		foreach( InteractablePassThrough pt in VRC_Auto.GetComponentsInChildren<InteractablePassThrough>() )
		{
			InteractablePassThrough temp = (InteractablePassThrough) pt;
			temp.interactable = this;
		}

		foreach( InteractablePassThrough pt in VRC_Manual.GetComponentsInChildren<InteractablePassThrough>() )
		{
			InteractablePassThrough temp = (InteractablePassThrough) pt;
			temp.interactable = this;
		}

		foreach( InteractablePassThrough pt in VRC_ExtManual.GetComponentsInChildren<InteractablePassThrough>() )
		{
			InteractablePassThrough temp = (InteractablePassThrough) pt;
			temp.interactable = this;
		}

		foreach( InteractablePassThrough pt in VRC_IOStatus.GetComponentsInChildren<InteractablePassThrough>() )
		{
			InteractablePassThrough temp = (InteractablePassThrough) pt;
			temp.interactable = this;
		}

		foreach( InteractablePassThrough pt in VRC_MachineParameters.GetComponentsInChildren<InteractablePassThrough>() )
		{
			InteractablePassThrough temp = (InteractablePassThrough) pt;
			temp.interactable = this;
		}

		foreach( InteractablePassThrough pt in VRC_OperatorSettings.GetComponentsInChildren<InteractablePassThrough>() )
		{
			InteractablePassThrough temp = (InteractablePassThrough) pt;
			temp.interactable = this;
		}

		foreach( InteractablePassThrough pt in VRC_SystemPreheat.GetComponentsInChildren<InteractablePassThrough>() )
		{
			InteractablePassThrough temp = (InteractablePassThrough) pt;
			temp.interactable = this;
		}

		//Parent them all to the frame
		VRC_StartupScreen.transform.parent = this.gameObject.transform;
		VRC_Auto.transform.parent = this.gameObject.transform;
		VRC_Manual.transform.parent = this.gameObject.transform;
		VRC_ExtManual.transform.parent = this.gameObject.transform;
		VRC_IOStatus.transform.parent = this.gameObject.transform;
		VRC_MachineParameters.transform.parent = this.gameObject.transform;
		VRC_OperatorSettings.transform.parent = this.gameObject.transform;
		VRC_SystemPreheat.transform.parent = this.gameObject.transform;

		//Offsets
		VRC_StartupScreen.transform.localPosition = 	offset;
		VRC_Auto.transform.localPosition = 				offset;
		VRC_Manual.transform.localPosition = 			offset;
		VRC_ExtManual.transform.localPosition = 		offset;
		VRC_IOStatus.transform.localPosition = 			offset;
		VRC_MachineParameters.transform.localPosition = offset;
		VRC_OperatorSettings.transform.localPosition = 	offset;
		VRC_SystemPreheat.transform.localPosition = 	offset;

		currentMenu = VRC_StartupScreen;

		VRC_StartupScreen.SetActive(false);
		VRC_Auto.SetActive(false);
		VRC_Manual.SetActive(false);
		VRC_ExtManual.SetActive(false);
		VRC_IOStatus.SetActive(false);
		VRC_MachineParameters.SetActive(false);
		VRC_OperatorSettings.SetActive(false);
		VRC_SystemPreheat.SetActive(false);
		currentMenu.SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void OnInteraction (RaycastHit hit)
	{
		//Switch to handle all the different menus and buttons?
		GameObject temp;
		switch(hit.collider.gameObject.name)
		{
		//For Startup Buttons
		case "VRC_StartupScreen_Button_Manual":
			goToMenu(VRC_Manual);
			break;
		case "VRC_StartupScreen_Button_Auto":
			goToMenu(VRC_Auto);
			break;
		//For Auto Buttons
		case "VRC_Auto_BottomMenu_StartUpScreen_00":
			goToMenu(VRC_StartupScreen);
				break;
		case "VRC_Auto_BottomMenu_OperatorInfo_01":
			break;
		case "VRC_Auto_BottomMenu_Recipes_02":
			break;
		case "VRC_Auto_BottomMenu_ExtManual_03":
			goToMenu(VRC_ExtManual);
			break;
		case "VRC_Auto_BottomMenu_Manual_04":
			goToMenu(VRC_Manual);
			break;
		case "VRC_Auto_BottomMenu_SystemPreheat_05":
			goToMenu (VRC_SystemPreheat);
			break;
		case "VRC_Auto_BottomMenu_OperatorSettings_06":
			goToMenu(VRC_OperatorSettings);
			break;
		case "VRC_Auto_BottomMenu_Back_07":
			goBack();
			break;

		case "VRC_Auto_Button_Auto":
			break;
		case "VRC_Auto_Button_Off":
			break;
		case "VRC_Auto_Button_DisableAll":
			break;

		//For Manual Buttons
		case "VRC_Manual_BottomMenu_StartUpScreen_00":
			goToMenu (VRC_StartupScreen);
			break;
		case "VRC_Manual_BottomMenu_SystemPreheat_01":
			goToMenu (VRC_SystemPreheat);
			break;
		case "VRC_Manual_BottomMenu_ExtManual_02":
			goToMenu (VRC_ExtManual);
			break;
		case "VRC_Manual_BottomMenu_Auto_03":
			goToMenu(VRC_Auto);
			break;
		case "VRC_Manual_BottomMenu_Recipes_04":
			break;
		case "VRC_Manual_BottomMenu_OperatorInfo_05":
			break;
		case "VRC_Manual_BottomMenu_OperatorSettings_06":
			goToMenu(VRC_OperatorSettings);
			break;
		case "VRC_Manual_BottomMenu_Back_07":
			goBack();
			break;

		//For ExtManual
		case "VRC_ExtManual_BottomMenu_SystemPreheat_00":
			goToMenu (VRC_SystemPreheat);
			break;
		case "VRC_ExtManual_BottomMenu_MachineParameters_01":
			goToMenu (VRC_MachineParameters);
			break;
		case "VRC_ExtManual_BottomMenu_SystemScreen_02":
			break;
		case "VRC_ExtManual_BottomMenu_IOStatus_03":
			goToMenu(VRC_IOStatus);
			break;
		case "VRC_ExtManual_BottomMenu_Manual_04":
			goToMenu(VRC_Manual);
			break;
		case "VRC_ExtManual_BottomMenu_Auto_05":
			goToMenu(VRC_Auto);
			break;
		case "VRC_ExtManual_BottomMenu_OperatorSettings_06":
			goToMenu (VRC_OperatorSettings);
			break;
		case "VRC_ExtManual_BottomMenu_Back_07":
			goBack();
			break;

		//IOStatus
		case "VRC_IOStatus_BottomMenu_StartUpScreen_00":
			goToMenu (VRC_StartupScreen);
			break;
		case "VRC_IOStatus_BottomMenu_ExtManual_01":
			goToMenu(VRC_ExtManual);
			break;
		case "VRC_IOStatus_BottomMenu_Auto_02":
			goToMenu (VRC_Auto);
			break;
		case "VRC_IOStatus_BottomMenu_OperatorSettings_06":
			goToMenu (VRC_OperatorSettings);
			break;
		case "VRC_IOStatus_BottomMenu_Back_07":
			goBack();
			break;

			//Machine Parameters
		case "VRC_MachineParameters_BottomMenu_StartUpScreen_00":
			goToMenu (VRC_StartupScreen);
			break;
		case "VRC_MachineParameters_BottomMenu_CalibrationScreen_01":
			break;
		case "VRC_MachineParameters_BottomMenu_ExtManual_02":
			goToMenu (VRC_ExtManual);
			break;
		case "VRC_MachineParameters_BottomMenu_Back_07":
			goBack();
			break;

			//Operator Settings
		case "VRC_OperatorSettings_BottomMenu_StartUpScreen_00":
			goToMenu (VRC_StartupScreen);
			break;
		case "VRC_OperatorSettings_BottomMenu_IOStatus_01":
			goToMenu (VRC_IOStatus);
			break;
		case "VRC_OperatorSettings_BottomMenu_Manual_02":
			goToMenu(VRC_Manual);
			break;
		case "VRC_OperatorSettings_BottomMenu_Auto_03":
			goToMenu (VRC_Auto);
			break;
		case "VRC_OperatorSettings_BottomMenu_Back_07":
			goBack();
			break;

			//System Preheat
		case "VRC_SystemPreheat_BottomMenu_StartUpScreen_00":
			goToMenu (VRC_StartupScreen);
			break;
		case "VRC_SystemPreheat_BottomMenu_IOStatus_01":
			goToMenu (VRC_IOStatus);
			break;
		case "VRC_SystemPreheat_BottomMenu_Manual_02":
			goToMenu (VRC_Manual);
			break;
		case "VRC_SystemPreheat_BottomMenu_Auto_03":
			goToMenu (VRC_Auto);
			break;
		case "VRC_SystemPreheat_BottomMenu_Recipes_04":
			break;
		case "VRC_SystemPreheat_BottomMenu_OperatorInfo_05":
			break;
		case "VRC_SystemPreheat_BottomMenu_OperatorSettings_06":
			goToMenu (VRC_OperatorSettings);
			break;
		case "VRC_SystemPreheat_BottomMenu_Back_07":
			goBack();
			break;

		}
	}

	protected void goToMenu(GameObject menu)
	{
		currentMenu.SetActive(false);
		previousMenu = currentMenu;
		currentMenu = menu;
		currentMenu.SetActive(true);
	}

	protected void goBack()
	{
		currentMenu.SetActive (false);
		GameObject temp = currentMenu;
		currentMenu = previousMenu;
		previousMenu = temp;
		currentMenu.SetActive (true);
	}

}