﻿using UnityEngine;
using System.Collections;

public class InventoryManager : MonoBehaviour {
	
	protected Item powder;
	protected Item nozzle;
	protected Item substrate; 
	
	public Texture iconPowderPlaceholder;
	public Texture iconNozzlePlaceholder;
	public Texture iconSubstratePlaceholder;
	public Texture iconPowder;
	public Texture iconNozzle;
	public Texture iconSubstrate;

	protected bool isCarryingPowder = false;
	protected bool isCarryingNozzle = false;
	protected bool isCarryingSubstrate = false;

	protected Rect GUIArea;

	protected Vector2 buffer;

	protected bool isBusy = false;

	protected float delayInSeconds = 0.25f;

	protected ObjectiveManager objectiveManager;

	// Use this for initialization
	void Start () {
		float maxWidth = Screen.width / 4.0f;
		float iconWidth = maxWidth / 4.0f;
		GUIArea = new Rect(0.0f, 0.0f, maxWidth, maxWidth);
		buffer = new Vector2(maxWidth / 12.0f, maxWidth / 12.0f);

		//Grab the objective manager
		objectiveManager = FindObjectOfType<ObjectiveManager>();
	}

	void OnGUI()
	{
		float maxWidth = Screen.width / 4.0f;
		float iconWidth = maxWidth / 4.0f;

		GUILayout.BeginArea(GUIArea);
		//Draw images
		GUILayout.BeginHorizontal();
		if( isCarryingPowder)
			GUI.Label(new Rect(buffer.x,buffer.y, iconWidth, iconWidth), iconPowder);
		else
			GUI.Label(new Rect(buffer.x,buffer.y, iconWidth, iconWidth), iconPowderPlaceholder);

		if(isCarryingNozzle)
			GUI.Label(new Rect(buffer.x + iconWidth,buffer.y, iconWidth, iconWidth),iconNozzle);
		else
			GUI.Label (new Rect(buffer.x + iconWidth,buffer.y, iconWidth, iconWidth),iconNozzlePlaceholder);

		if(isCarryingSubstrate)
			GUI.Label(new Rect(buffer.x + iconWidth*2,buffer.y, iconWidth, iconWidth),iconSubstrate);
		else
			GUI.Label(new Rect(buffer.x + iconWidth*2,buffer.y, iconWidth, iconWidth),iconSubstratePlaceholder);
	
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
	}

	/// <summary>
	/// Does the object allow for storing?
	/// </summary>
	/// <param name="gameObject">Game object.</param>
	public void CheckRay( RaycastHit hit)
	{
		if(!isBusy && Input.GetKey(KeyCode.E))
		{
			Debug.Log("InventoryManager.CheckRay");

			//Handle Item
			
			Item item = (Item) hit.collider.gameObject.GetComponent<Item>();
			
			if(item != null)
			{
				AddItem(item);
				return;
			}

			//Handle Silhouette

			Silhouette silhouette = (Silhouette)hit.collider.gameObject.GetComponent<Silhouette>();
			
			if(silhouette != null)
			{
				switch( silhouette.Storage.type)
				{
				case ItemType.Powder:
					if(isCarryingPowder)
						silhouette.Storage.Add(silhouette, RemovePowder());
					break;
				case ItemType.Nozzle:
					if(isCarryingNozzle)
						silhouette.Storage.Add(silhouette, RemoveNozzle());
					break;
				case ItemType.Substrate:
					if(isCarryingPowder)
						silhouette.Storage.Add(silhouette, RemoveSubstrate());
					break;
				case ItemType.All:
					break;
				}
			}
		}
	}

	public bool AddItem( Item item)
	{

		Debug.Log("AddItem");

		switch(item.Type)
		{
			case ItemType.Powder:
				return AddPowder(item);
				break;
			case ItemType.Nozzle:
				return AddNozzle(item);
				break;
			case ItemType.Substrate:
				return AddSubstrate(item);
				break;
		}
		return false;
	}

	public bool AddPowder(Item powder)
	{
		Debug.Log("AddPowder");
		if(!isCarryingPowder && powder.Storage.Remove(powder))
		{
			this.powder = powder;
			this.powder.gameObject.SetActive(false);
			this.isCarryingPowder = true;

			objectiveManager.completeObjective("powder_select");

			StartCoroutine("busyDelay");
			return true;
		}
		return false;
	}
	
	public Item RemovePowder()
	{
		if(this.powder == null)
			return null;

		Item temp = this.powder;
		this.powder = null;
		this.isCarryingPowder = false;
		StartCoroutine("busyDelay");
		return temp;
	}

	public bool AddNozzle( Item nozzle)
	{
		if(!isCarryingNozzle && nozzle.Storage.Remove(nozzle))
		{
			this.nozzle = nozzle;
			this.nozzle.gameObject.SetActive(false);
			this.isCarryingNozzle = true;

			objectiveManager.completeObjective("select_nozzle");

			StartCoroutine("busyDelay");
			return true;
		}
		return false;
	}

	public Item RemoveNozzle()
	{
		if(this.nozzle == null)
			return null;
		Item temp = nozzle;
		nozzle = null;
		this.isCarryingNozzle = false;
		StartCoroutine("busyDelay");
		return temp;

	}

	public bool AddSubstrate( Item substrate)
	{
		if(!isCarryingSubstrate  && substrate.Storage.Remove(substrate))
		{
			this.substrate = substrate;
			this.substrate.gameObject.SetActive(false);
			this.isCarryingSubstrate = true;

			objectiveManager.completeObjective("substrate_choose");

			StartCoroutine("busyDelay");
			return true;
		}
		return false;
	}

	public Item RemoveSubstrate()
	{

		if(this.substrate == null)
			return null;

		Item temp = substrate;
		substrate = null;
		this.isCarryingSubstrate = false;
		StartCoroutine("busyDelay");
		return temp;
	}

	IEnumerator busyDelay()
	{
		isBusy = true;
		yield return new WaitForSeconds(delayInSeconds);
		isBusy = false;
	}

	public bool IsCarryingPowder {
		get {
			return this.isCarryingPowder;
		}
	}

	public bool IsCarryingNozzle {
		get {
			return this.isCarryingNozzle;
		}
	}

	public bool IsCarryingSubstrate {
		get {
			return this.isCarryingSubstrate;
		}
	}

	public Item Powder {
		get {
			return this.powder;
		}
	}

	public Item Nozzle {
		get {
			return this.nozzle;
		}
	}

	public Item Substrate {
		get {
			return this.substrate;
		}
	}

}
