﻿using UnityEngine;
using System.Collections;

public enum ItemType{
	Powder,
	Nozzle,
	Substrate,
	All
}

public class Item : MonoBehaviour {
	
	protected Storage storage;	//The object the item is currently stored within.

	public ItemType type;

	// Use this for initialization
	void Start () {
	}

	public Storage Storage {
			get {
				return this.storage;
			}
			set {
				storage = value;
			}
		}

	public ItemType Type {
		get {
			return this.type;
		}
		set {
			type = value;
		}
	}

}
