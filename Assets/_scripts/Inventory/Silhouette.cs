﻿using UnityEngine;
using System.Collections;

public class Silhouette : MonoBehaviour {

	protected Storage storage;

	// Update is called once per frame
	void Update () {
		gameObject.transform.LookAt(Camera.main.transform.position);
	}


	public Storage Storage {
		get {
			return this.storage;
		}
		set {
			storage = value;
		}
	}

}
