﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Storage : MonoBehaviour {

	protected ItemType itemType;

	public int size = 4;
	protected int freeSpace;
	public GameObject silhouette;

	protected GameObject[] storageLocations;
	protected Item[] storedItems;
	protected Silhouette[] silhouettes;

	/// <summary>
	/// Used in start to grab all the storage locations.
	/// </summary>
	protected abstract void GetStorageLocations();
	protected abstract void FillWithSilhouttes();

	public void Start()
	{
		itemType = ItemType.All;
		freeSpace = size;
		storageLocations = new GameObject[size];
		storedItems = new Item[size];
		silhouettes = new Silhouette[size];
		GetStorageLocations();
		FillWithSilhouttes();
	}

	/// <summary>
	/// Sets the object to be a child of the storage location and resets the local position to zero.
	/// </summary>
	protected void PositionItem( int index)
	{
		storedItems[index].gameObject.transform.parent = storageLocations[index].transform;
		storedItems[index].gameObject.transform.localPosition = Vector3.zero;
		storedItems[index].gameObject.transform.localRotation = new Quaternion();
	}

	protected void PositionSilhouette( int index)
	{
		silhouettes[index].gameObject.transform.parent = storageLocations[index].transform;
		silhouettes[index].gameObject.transform.localPosition = Vector3.zero;
		silhouettes[index].gameObject.transform.localRotation = new Quaternion();
	}

	public bool IsFull()
	{
		if( freeSpace == 0)
		{
			return true;
		}

		return false;
	}

	protected int FindItemIndex(Item item)
	{
		for( int i = 0; i < size; i++)
		{
			if( storedItems[i].GetInstanceID() == item.GetInstanceID())
			{
				return i;
			}
		}
		
		return -1;
	}

	protected int FindSilhouetteIndex( Silhouette silhouette)
	{
		for( int i = 0; i < size; i++)
		{
			if( silhouettes[i].GetInstanceID() == silhouette.GetInstanceID())
			{
				return i;
			}
		}

		return -1;

	}

	protected bool Add(int index, Item item)
	{
		if(IsFull())
			return false;
		
		silhouettes[index].gameObject.SetActive(false);
		item.gameObject.SetActive(true);
		storedItems[index] = item;
		PositionItem(index);
		
		freeSpace--;
		
		return true;
	}

	/// <summary>
	/// Replace the object based on the silhouette pressed
	/// </summary>
	/// <param name="silhouette">Silhouette.</param>
	/// <param name="item">Item.</param>
	public bool Add( Silhouette silhouette, Item item)
	{
		if(IsFull())
			return false;

		Debug.Log ("Storage Add: " + silhouette.name + " " + item.name);

		int index = FindSilhouetteIndex(silhouette);

		if(index == -1)
		{
			return false;
			Debug.Log ("Storage.Add, invalud silhouette passed");
		}
			
		silhouettes[index].gameObject.SetActive(false);
		item.gameObject.SetActive(true);
		storedItems[index] = item;
		PositionItem(index);
		item.Storage = this;
		freeSpace--;

		return true;
	}
	
	public Item Remove( Item item)
	{
		int index = FindItemIndex(item);
		
		if( index == -1)
		{
			Debug.Log ("Storage.Remove, invalid item passed");
			return null;
		}

		Item tempItem = storedItems[index];
		storedItems[index] = null;
		silhouettes[index].gameObject.SetActive(true);

		freeSpace++;
		return tempItem;
	}

	public ItemType type {
		get {
			return this.itemType;
		}
		set {
			itemType = value;
		}
	}

}
