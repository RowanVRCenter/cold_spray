﻿using UnityEngine;
using System.Collections;

public class Storage_Cabinet : Storage {

	public GameObject powderAl;
	public GameObject powderCu;
	public GameObject powderTa;
	public GameObject powderTi;


	// Use this for initialization
	void Start () {
		base.Start();

		//Fill with Powders
		powderAl = (GameObject)Instantiate(powderAl);
		powderCu = (GameObject)Instantiate(powderCu);
		powderTa = (GameObject)Instantiate(powderTa);
		powderTi = (GameObject)Instantiate(powderTi);

		Add (0, powderAl.GetComponent<Item>());
		Add (1, powderCu.GetComponent<Item>());
		Add (2, powderTa.GetComponent<Item>());
		Add (3, powderTi.GetComponent<Item>());

		storedItems[0].Storage = this;
		storedItems[1].Storage = this;
		storedItems[2].Storage = this;
		storedItems[3].Storage = this;

		itemType = ItemType.Powder;
	}


	public bool Add( Silhouette silhouette, Item item)
	{
		if(  item.Type != ItemType.Powder)
		{
			return false;
		}

		return base.Add (silhouette, item);

	}

	protected override void FillWithSilhouttes ()
	{
		for(int i = 0; i < size; i++)
		{
			GameObject temp = (GameObject)Instantiate(silhouette);
			silhouettes[i] = temp.GetComponent<Silhouette>();
			silhouettes[i].Storage = this;
			PositionSilhouette(i);
		}
	}

	protected override void GetStorageLocations ()
	{
		storageLocations[0] = GameObject.Find("Inventory1").gameObject;
		storageLocations[1] = GameObject.Find("Inventory2").gameObject;
		storageLocations[2] = GameObject.Find("Inventory3").gameObject;
		storageLocations[3] = GameObject.Find("Inventory4").gameObject;
	}


}
