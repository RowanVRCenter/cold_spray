﻿using UnityEngine;
using System.Collections;

public class Storage_NozzleTable : Storage {

	public GameObject nozzleExpansionRatio4;
	public GameObject nozzleExpansionRatio12;

	// Use this for initialization
	void Start () {
		base.Start();

		//Fill with Nozzles
		nozzleExpansionRatio4 = (GameObject)Instantiate(nozzleExpansionRatio4);
		nozzleExpansionRatio12 = (GameObject)Instantiate(nozzleExpansionRatio12);
		
		Add (0, nozzleExpansionRatio4.GetComponent<Item>());
		Add (1, nozzleExpansionRatio12.GetComponent<Item>());
		
		storedItems[0].Storage = this;
		storedItems[1].Storage = this;

		itemType = ItemType.Nozzle;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public bool Add( Silhouette silhouette, Item item)
	{
		if(  item.Type != ItemType.Nozzle)
		{
			return false;
		}
		
		return base.Add (silhouette, item);
	}

	protected override void GetStorageLocations ()
	{
		storageLocations[0] = GameObject.Find("NozzleTableInventory1").gameObject;
		storageLocations[1] = GameObject.Find("NozzleTableInventory2").gameObject;
	}

	protected override void FillWithSilhouttes ()
	{
		for(int i = 0; i < size; i++)
		{
			GameObject temp = (GameObject)Instantiate(silhouette);
			silhouettes[i] = temp.GetComponent<Silhouette>();
			silhouettes[i].Storage = this;
			PositionSilhouette(i);
		}
	}

}
