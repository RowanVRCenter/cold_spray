﻿using UnityEngine;
using System.Collections;

public class Storage_Oven : Storage {
	

	// Use this for initialization
	void Start () {
		base.Start();
		itemType = ItemType.Powder;

	}

	protected override void FillWithSilhouttes ()
	{
		for(int i = 0; i < size; i++)
		{
			GameObject temp = (GameObject)Instantiate(silhouette);
			silhouettes[i] = temp.GetComponent<Silhouette>();
			silhouettes[i].Storage = this;
			PositionSilhouette(i);
		}
	}

	public bool Add( Silhouette silhouette, Item item)
	{
		if(  item.Type != ItemType.Powder)
		{
			return false;
		}

		return base.Add (silhouette, item);
	}

	protected override void GetStorageLocations ()
	{
		storageLocations[0] = GameObject.Find("OvenInventory1").gameObject;
	}

}
