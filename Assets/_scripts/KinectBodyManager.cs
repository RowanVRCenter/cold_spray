using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Windows.Kinect;

public class KinectBodyManager : MonoBehaviour 
{
	private KinectSensor sensor;
	private BodyFrameReader reader;
	private IList<Body> bodies = null;

	public IList<Body> GetData()
	{
		return bodies;
	}

	void Start()
	{
		sensor = KinectSensor.GetDefault();

		if (sensor != null) 
		{
			if (!sensor.IsOpen)
			{
				sensor.Open();
				Debug.Log("Sensor opened");
			}

			reader = sensor.BodyFrameSource.OpenReader();
			Debug.Log ("Reader opened");
		}

		reader.FrameArrived += this.OnBodyFrameArrived;
	}

	void OnApplicationQuit()
	{
		if(reader != null)
		{
			reader.Dispose();
		}
		if(sensor != null && sensor.IsOpen)
		{
			sensor.Close();
			sensor = null;
		}
	}

	void OnBodyFrameArrived (object sender, BodyFrameArrivedEventArgs e)
	{
		using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame())
		{
			if (bodyFrame != null)
			{
				if (this.bodies == null)
				{
					this.bodies = new Body[bodyFrame.BodyCount];
				}
				bodyFrame.GetAndRefreshBodyData(this.bodies);
			}
		}
	}
}