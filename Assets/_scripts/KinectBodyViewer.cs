﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;

public class KinectBodyViewer : MonoBehaviour 
{
	public GameObject BodyManagerGameObject;
    public GameObject Head;

	private KinectBodyManager BodyManager; 
	private IList<Kinect.Body> bodies = null;
	 
	private bool MainUserBodyFound = false;
	private ulong MainUserTrackingID = 0;
	private GameObject MainUserGameObject;

    private List<Kinect.JointType> BoneList = new List<Kinect.JointType>()
    {
        Kinect.JointType.SpineShoulder,
        Kinect.JointType.ShoulderRight,
        Kinect.JointType.ElbowRight,
        Kinect.JointType.WristRight,
        Kinect.JointType.HandRight,
        Kinect.JointType.ThumbRight,
        Kinect.JointType.HandTipRight
    };

	// Update is called once per frame
	void Update () 
	{
		BodyManager = BodyManagerGameObject.GetComponent<KinectBodyManager>();
		bodies = BodyManager.GetData();

		if (bodies != null)
		{
			foreach (Kinect.Body body in bodies) 
			{
				if(!MainUserBodyFound && (body.TrackingId > 0))
				{
					MainUserTrackingID = body.TrackingId;
					MainUserBodyFound = true;
					MainUserGameObject = CreateBodyGameObject(body);
				}
				else if (MainUserBodyFound && (body.TrackingId == MainUserTrackingID))
				{
					//Update body data 
					RefreshBodyGameObject(body, MainUserGameObject);
				}
			}	
		}
	}

	GameObject CreateBodyGameObject(Kinect.Body body)
	{
		GameObject bodyGameObject = new GameObject("MainUser: " + body.TrackingId.ToString());
        bodyGameObject.transform.position = Head.transform.position;
        bodyGameObject.transform.parent = Head.transform;

        //bodyGameObject.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++) 
		{
			if(BoneList.Contains(jt))
			{
				GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                
				jointObj.name = jt.ToString();
				jointObj.transform.parent = bodyGameObject.transform;
                if (jt == Kinect.JointType.ShoulderRight)
                {
                    jointObj.transform.position = bodyGameObject.transform.position;
                }
                else
                {
                    Transform shoulderRight = bodyGameObject.transform.FindChild(Kinect.JointType.ShoulderRight.ToString());
                    jointObj.transform.parent = shoulderRight.transform;
                    Vector3 jointPos = new Vector3(body.Joints[jt].Position.X, body.Joints[jt].Position.Y, body.Joints[jt].Position.Z);
                    jointObj.transform.localPosition = jointPos;
                    jointObj.transform.localRotation = new Quaternion(body.JointOrientations[jt].Orientation.X, body.JointOrientations[jt].Orientation.Y,
                                                                 body.JointOrientations[jt].Orientation.Z, body.JointOrientations[jt].Orientation.W);
                    jointObj.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                }
				
			}
		}

		return bodyGameObject;
	}

    void RefreshBodyGameObject(Kinect.Body body, GameObject bodyObject)
    {
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            if (BoneList.Contains(jt) && jt != Kinect.JointType.ShoulderRight)
            {
                Transform shoulderRight = bodyObject.transform.FindChild(Kinect.JointType.ShoulderRight.ToString());
                Transform jointObj = shoulderRight.transform.FindChild(jt.ToString());
                Vector3 jointPos = new Vector3(body.Joints[jt].Position.X, body.Joints[jt].Position.Y, body.Joints[jt].Position.Z);
                jointObj.localPosition = jointPos;
            }
        }
    }
}