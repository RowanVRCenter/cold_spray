﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

	public bool isUp;
	public GameObject startLocation;
	public GameObject endLocation;

	private float speed = 2.0f;
	private float increment = 0.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetMouseButton (0))
		{
			increment += Time.deltaTime * speed;
		}
		else
		{
			increment -= Time.deltaTime * speed;
		}

		if(increment >= 1)
			increment = 1;
		else if(increment <= 0)
			increment = 0;

		transform.position = Vector3.Lerp(startLocation.transform.position, endLocation.transform.position, increment);

	}
}
