﻿using UnityEngine;
using System.Collections;


abstract class Objectable : MonoBehaviour
{
	public string objectiveName;
	private ObjectiveManager objectiveManager;

	// Use this for initialization
	public void Start () {
		//Find the manager
		objectiveManager = (ObjectiveManager)FindObjectOfType<ObjectiveManager>();
	}

	/// <summary>
	/// All objective conditions to check code should be placed in here
	/// </summary>
	protected abstract void checkConditions();

}
