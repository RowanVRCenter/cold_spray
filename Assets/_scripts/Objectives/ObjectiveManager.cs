﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.Xml;
using System.IO;

public class Objective
{
	private string name = "default_name";
	private string description = "Default Description.";
	private int order = 0;
	private bool isComplete = false;

	public string Name {
		get {
			return this.name;
		}
		set {
			name = value;
		}
	}

	public string Description {
		get {
			return this.description;
		}
		set {
			description = value;
		}
	}

	public bool IsComplete {
		get {
			return this.isComplete;
		}
		set {
			isComplete = value;
		}
	}

	public int Order {
		get {
			return this.order;
		}
		set {
			order = value;
		}
	}

}

public class ObjectiveManager: MonoBehaviour
{
	public TextAsset objectivesTextData;
	private Dictionary<string, Objective> objectiveList;
	
	private int objectivesCompletedCount = 0;
	private int numberObjectives;

	private List<Objective> currentObjectives;

	private int currentObjective = 0;

	private Rect guiArea;

	void Start()
	{
		LoadObjectives loadObjectives = new LoadObjectives( );
		objectiveList = loadObjectives.ParseObjectivesXML(objectivesTextData.text);
		numberObjectives = objectiveList.Count;
		guiArea = new Rect( Screen.width - (Screen.width / 4.0f), 0.0f, Screen.width / 4.0f, Screen.height / 3.0f );
	}

	void OnGUI()
	{
		GUILayout.BeginArea(guiArea);
		GUILayout.BeginVertical();
		GUILayout.Label("Objectives:");
		GUILayout.Label("Current: " );
		GUILayout.Label("Complete: " + ObjectivesCompletedCount.ToString());
		GUILayout.EndVertical();
		GUILayout.EndArea();
	}

	/// <summary>
	/// Attempts to complete the objective
	/// </summary>
	/// <param name="objectiveName">Objective name.</param>
	public bool completeObjective( string objectiveName)
	{
		Objective objective = objectiveList[objectiveName];

		if(objective == null)
			return false;

		if(objective.IsComplete)
			return false;

		objective.IsComplete = true;

		ObjectivesCompletedCount++;

		return true;
	}

	int ObjectivesCompletedCount {
		get {
			return this.objectivesCompletedCount;
		}
		set {
			objectivesCompletedCount = value;
		}
	}
	
	public int getObjectiveCount()
	{
		return objectiveList.Count;
	}

}

public class LoadObjectives {

	public LoadObjectives()
	{
	}

	public Dictionary<string, Objective> ParseObjectivesXML( string xmlData)
	{
		XmlDocument xmlDocument = new XmlDocument();
		xmlDocument.Load ( new StringReader(xmlData) );

		Dictionary<string, Objective> objectives = new Dictionary<string, Objective>();

		//Gets all objectives from root objectiveList
		string xmlPathPattern = "//objectiveList/objective";

		XmlNodeList nodeList = xmlDocument.SelectNodes( xmlPathPattern );

		foreach( XmlNode node in nodeList)
		{
			Objective objective = xmlToObjective(node);
			Debug.Log(objective.Name + " " + objective.Description);
			objectives.Add( objective.Name, objective );
		}

		return objectives;
	}

	private Objective xmlToObjective(XmlNode node)
	{
		Objective objective = new Objective();

		foreach (XmlNode childNode in node.ChildNodes)
		{

			Debug.Log (childNode.Name);
			Debug.Log (childNode.InnerText);
			switch( childNode.Name )
			{
				case "name":
				objective.Name = childNode.InnerText;
					break;
				case "description":
				objective.Description = childNode.InnerText;
					break;
				case "order":
				objective.Order =  int.Parse(childNode.InnerText);
					break;
			}
		}

		return objective;
	}


}
