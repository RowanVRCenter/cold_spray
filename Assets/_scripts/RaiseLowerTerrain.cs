﻿using UnityEngine;
using System.Collections;

public class RaiseLowerTerrain : MonoBehaviour
{
	public float SprayIntensity = 0.002f;
	public int SprayRadius = 100;
	public float AlphaMultiplier = 20f;

    private Valve.VR.EVRButtonId gripButton = Valve.VR.EVRButtonId.k_EButton_Grip;
    public bool gripButtonDown = false;

    private Valve.VR.EVRButtonId triggerButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
    public bool triggerButtonDown = false;

    private SteamVR_Controller.Device controller { get { return SteamVR_Controller.Input((int)trackedObject.index); } }
    private SteamVR_TrackedObject trackedObject;

	private int AlphaRadius;
	private Terrain myTerrain;
	private int xResolution;
	private int zResolution;
	private float[,] heightMapBackup;
	private float[,] HeightAdd;
	private float[,,] AlphaBlend;
	protected const float DEPTH_METER_CONVERT=0.05f;
	protected const float TEXTURE_SIZE_MULTIPLIER = 1.25f;
	public int DeformationTextureNum = 1;
	protected int alphaMapWidth;
	protected int alphaMapHeight;
	protected int numOfAlphaLayers;
	private float[, ,] alphaMapBackup;
	
	void Start()
	{
        trackedObject = GetComponent<SteamVR_TrackedObject>();

		myTerrain = GetComponent<Terrain>();
		xResolution = myTerrain.terrainData.heightmapWidth;
		zResolution = myTerrain.terrainData.heightmapHeight;
		alphaMapWidth = myTerrain.terrainData.alphamapWidth;
		alphaMapHeight = myTerrain.terrainData.alphamapHeight;
		numOfAlphaLayers = myTerrain.terrainData.alphamapLayers;
		HeightAdd = new float[SprayRadius * 2, SprayRadius * 2];
		AlphaRadius = SprayRadius * alphaMapHeight/zResolution;
		AlphaBlend = new float[AlphaRadius * 2, AlphaRadius * 2, numOfAlphaLayers];

		for (int i = 0; i < SprayRadius*2; i++){
			for (int j = 0; j < SprayRadius*2; j++){
				float distance = Mathf.Sqrt((i-SprayRadius)*(i-SprayRadius)+(j-SprayRadius)*(j-SprayRadius));
				if (distance < SprayRadius){
					HeightAdd[i,j] = SprayIntensity*1f/4f*Mathf.Cos(((distance)/(SprayRadius))*Mathf.PI)+SprayIntensity*1f/4f;
				}else{
					HeightAdd[i,j] = 0;
				}
			}
		}
		for (int i = 0; i < AlphaRadius*2; i++){
			for (int j = 0; j < AlphaRadius*2; j++){
				float distance = Mathf.Sqrt((i-AlphaRadius)*(i-AlphaRadius)+(j-AlphaRadius)*(j-AlphaRadius));
				for (int k = 0; k < numOfAlphaLayers; k++){
					if (k < (numOfAlphaLayers-1)){
						if (distance < AlphaRadius){
							AlphaBlend[i,j,k] = 0f-(SprayIntensity*AlphaMultiplier*Mathf.Cos(((distance)/(SprayRadius))*Mathf.PI)+SprayIntensity*AlphaMultiplier);
						}else{
							AlphaBlend[i,j,k] = 0f;
						}
					}else{
						if (distance < AlphaRadius){
							AlphaBlend[i,j,k] =SprayIntensity*AlphaMultiplier*Mathf.Cos(((distance)/(SprayRadius))*Mathf.PI)+SprayIntensity*AlphaMultiplier;
						}else{
							AlphaBlend[i,j,k] = 0f;
						}
					}
				}
			}
		}    
		heightMapBackup = myTerrain.terrainData.GetHeights(0, 0, xResolution, zResolution);
		alphaMapBackup = myTerrain.terrainData.GetAlphamaps(0, 0, alphaMapWidth, alphaMapHeight);
		
	}
	
	void OnApplicationQuit()
	{
		myTerrain.terrainData.SetHeights(0, 0, heightMapBackup);
		myTerrain.terrainData.SetAlphamaps(0, 0, alphaMapBackup);
	}
	
	
	void Update()
	{
        gripButtonDown = controller.GetPressDown(gripButton);
        triggerButtonDown = controller.GetPressDown(triggerButton);
	}
	
	
	private void raiselowerTerrainArea(Vector3 point, int radius, float[,] heightAdd)
	{
		Vector3 LocalTerrainPos = GetNormalizedPositionRelativeToTerrain(point, myTerrain);
		int terX =(int)((LocalTerrainPos.x * xResolution)-radius);
		int terZ =(int)((LocalTerrainPos.z * zResolution)-radius);
		if (terX < 0) terX = 0;
		if (terX+(radius*2) > xResolution)    terX = xResolution-radius*2;
		if (terZ < 0) terZ = 0;
		if (terZ+(radius*2) > zResolution)    terZ = zResolution-radius*2;
		float[,] heights = myTerrain.terrainData.GetHeights(terX, terZ, radius*2, radius*2);
		for (int i = 0; i < radius*2; i++){
			for (int j = 0; j < radius*2; j++){
				heights[i,j] += heightAdd[i,j];
				if (heights[i,j] > 1f){
					heights[i,j] = 1;
				}
			}
		}
		myTerrain.terrainData.SetHeights (terX, terZ, heights);
	}
	
	protected void TextureDeformation(Vector3 pos, int radius,int textureIDnum)
	{
		Vector3 alphaMapTerrainPos = GetRelativeTerrainPositionFromPos(pos, myTerrain, alphaMapWidth, alphaMapHeight);
		int alphaMapStartPosX = (int)(alphaMapTerrainPos.x - radius);
		int alphaMapStartPosZ = (int)(alphaMapTerrainPos.z - radius);
		if (alphaMapStartPosX < 0) alphaMapStartPosX = 0;
		if (alphaMapStartPosX+(radius*2)+1 > alphaMapWidth)    alphaMapStartPosX = alphaMapWidth-radius*2;
		if (alphaMapStartPosZ < 0) alphaMapStartPosZ = 0;
		if (alphaMapStartPosZ+(radius*2)+1 > alphaMapHeight)    alphaMapStartPosZ = alphaMapHeight-radius*2;
		float[, ,] alphas = myTerrain.terrainData.GetAlphamaps(alphaMapStartPosX, alphaMapStartPosZ, radius * 2, radius * 2);
		for (int i = 0; i < radius*2; i++){
			for (int j = 0; j < radius*2; j++){
				for (int k = 0; k < numOfAlphaLayers; k++){
					alphas[i,j,k] += AlphaBlend[i,j,k];
					if (alphas[i,j,k] < 0){
						alphas[i,j,k] = 0;
					}else if (alphas[i,j,k] > 1){
						alphas[i,j,k] = 1;
					}
				}
			}
		}
		myTerrain.terrainData.SetAlphamaps(alphaMapStartPosX, alphaMapStartPosZ, alphas);
	}
     
	void moveTerrain()
	{
		RaycastHit hit;
        //Ray ray = device.getRay();
        Ray ray = new Ray(transform.position, transform.forward);

        if (Physics.Raycast (ray, out hit)) 
		{
			if (hit.transform.gameObject == this.transform.gameObject){
				// area middle point x and z, radius, area height adjust
				raiselowerTerrainArea (hit.point, SprayRadius, HeightAdd); 
				// area middle point x and z, area size, texture ID from terrain textures
				TextureDeformation (hit.point, AlphaRadius, DeformationTextureNum);
			}
		}
	}
	
	protected Vector3 GetNormalizedPositionRelativeToTerrain(Vector3 pos, Terrain terrain)
	{
		Vector3 tempCoord = (pos - terrain.gameObject.transform.position);
		Vector3 coord;
		coord.x = tempCoord.x / myTerrain.terrainData.size.x;
		coord.y = tempCoord.y / myTerrain.terrainData.size.y;
		coord.z = tempCoord.z / myTerrain.terrainData.size.z;
		return coord;
	}
	
	protected Vector3 GetRelativeTerrainPositionFromPos(Vector3 pos,Terrain terrain, int mapWidth, int mapHeight)
	{
		Vector3 coord = GetNormalizedPositionRelativeToTerrain(pos, terrain);
		return new Vector3((coord.x * mapWidth), 0, (coord.z * mapHeight));
	}     
}