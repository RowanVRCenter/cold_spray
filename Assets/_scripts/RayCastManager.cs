﻿using UnityEngine;
using System.Collections;

public class RayCastManager : MonoBehaviour {
	
	public GameObject root;
	public float distance= 10.0f;

	public GetReal3DDevice device;

	public InteractableManager interactableManager;
	public InventoryManager inventoryManager;
	public InformationManager informationManager;

	private GameObject debugSphere;

	// Use this for initialization
	void Start () {
		debugSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		Destroy(debugSphere.GetComponent<SphereCollider>());
		debugSphere.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
	}
	
	// Update is called once per frame
	void Update ()
	{
		Ray ray = device.getRay();
		RaycastHit raycasthit;
		if (Physics.Raycast(ray, out raycasthit, distance))
		{
			debugSphere.transform.position = raycasthit.point;
			interactableManager.CheckRay( raycasthit );
			inventoryManager.CheckRay(raycasthit);
			informationManager.CheckRay(raycasthit);
		}
		else
		{
			informationManager.NoHit();
		}

	}
}