﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ControllerPass
{
    public SteamVR_TrackedObject obj;
    public enum controllerState {Enter, Exit};
    public controllerState state;
    public GameObject model;
}
