﻿using UnityEngine;
using System.Collections;

public class HandModelEditor : MonoBehaviour
{
    [SerializeField]
    private SteamVR_TrackedController controller;

    [SerializeField]
    private Mesh[] meshes;
    //0 = Neutral, 1 = Fist, 2 = Hold, 3 = Point.

    private bool canChange;
    
    void Awake()
    {
        canChange = true;
        
        controller.Gripped += new ClickedEventHandler(GripDown);
        controller.PadClicked += new ClickedEventHandler(PadDown);
        controller.TriggerClicked += new ClickedEventHandler(TriggerDown);

        controller.Ungripped += new ClickedEventHandler(ReturnHand);
        controller.PadUnclicked += new ClickedEventHandler(ReturnHand);
        controller.TriggerUnclicked += new ClickedEventHandler(ReturnHand);
    }
    
    /*
    public void SetModel(string modelName)
    {
        switch (modelName)
        {
            case "Neutral":
                gameObject.GetComponent<MeshFilter>().mesh = meshes[0];
                //gameObject.GetComponent<MeshCollider>().sharedMesh = meshes[0];
                break;
            case "Fist":
                gameObject.GetComponent<MeshFilter>().mesh = meshes[1];
                //gameObject.GetComponent<MeshCollider>().sharedMesh = meshes[1];  
                break;
            case "Hold":
                gameObject.GetComponent<MeshFilter>().mesh = meshes[2];
                //gameObject.GetComponent<MeshCollider>().sharedMesh = meshes[2];
                break;
            case "Point":
                gameObject.GetComponent<MeshFilter>().mesh = meshes[3];
                //gameObject.GetComponent<MeshCollider>().sharedMesh = meshes[3];
                break;
            default:
                Debug.LogError("Error,  model " + modelName + " not found.");
                break;
        }
    }
    */
    
    private void GripDown(object sender, ClickedEventArgs e)
    {
        if(canChange)
            gameObject.GetComponent<MeshFilter>().mesh = meshes[2];

        canChange = false;
    }

    private void PadDown(object sender, ClickedEventArgs e)
    {
        if(canChange)
            gameObject.GetComponent<MeshFilter>().mesh = meshes[3];

        canChange = false;
    }

    private void TriggerDown(object sender, ClickedEventArgs e)
    {
        if(canChange)
            gameObject.GetComponent<MeshFilter>().mesh = meshes[1];

        canChange = false;
    }
   
    private void ReturnHand(object sender, ClickedEventArgs e)
    {
        gameObject.GetComponent<MeshFilter>().mesh = meshes[0];
        canChange = true;
    }  
}
