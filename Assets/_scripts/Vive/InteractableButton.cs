﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InteractableButton : MonoBehaviour
{
    SteamVR_TrackedController trackedController;
    SteamVR_TrackedObject trackedObj;

    [SerializeField]
    private GameObject screen;

    public void Interact(ControllerPass pass)
    {
        if (pass.state == ControllerPass.controllerState.Enter)
        {
            trackedController = pass.obj.gameObject.GetComponentInChildren<SteamVR_TrackedController>();
            trackedController.PadClicked += new ClickedEventHandler(DoPadDown);
            //pass.obj.gameObject.GetComponentInChildren<HandModelEditor>().SetModel("Point");
            this.GetComponent<ParticleSystem>().Play();
        }
        else if (pass.state == ControllerPass.controllerState.Exit)
        {
            trackedController = pass.obj.gameObject.GetComponentInChildren<SteamVR_TrackedController>();
            trackedController.PadClicked -= new ClickedEventHandler(DoPadDown);
            //pass.obj.gameObject.GetComponentInChildren<HandModelEditor>().SetModel("Neutral");
            this.GetComponent<ParticleSystem>().Stop();
        }
    }

    void DoPadDown(object sender, ClickedEventArgs e)
    {
        this.GetComponent<ParticleSystem>().Stop();
        this.GetComponent<Transform>().Translate(-.005f, 0f, 0f);

        ((SteamVR_TrackedController)sender).PadUnclicked += new ClickedEventHandler(DoPadUp);
    }

    void DoPadUp(object sender, ClickedEventArgs e)
    {
        this.GetComponent<Transform>().Translate(.005f, 0f, 0f);

        ((SteamVR_TrackedController)sender).PadUnclicked -= new ClickedEventHandler(DoPadUp);
    }
}