﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InteractableDoor : MonoBehaviour
{
    SteamVR_TrackedController trackedController;
    SteamVR_TrackedObject trackedObj;
    ConfigurableJoint[] joints = new ConfigurableJoint[15];

    SoftJointLimit softJointLimit = new SoftJointLimit();
    SoftJointLimitSpring softJointLimitSpring = new SoftJointLimitSpring();

    ParticleSystem ps; //Making ParticleSystem a var because it is attached to a child. Looks better.

    void Awake()
    {
        softJointLimit.bounciness = 0;
        softJointLimit.contactDistance = 0.05f;
        softJointLimit.limit = 0;
        softJointLimitSpring.damper = 100;
        softJointLimitSpring.spring = 100;
        ps = this.GetComponentInChildren<ParticleSystem>();
    }

    void FixedUpdate()
    {
        //Door ranges go from 0->90 and -90->0
        if ((this.GetComponent<Transform>().eulerAngles.y == 30f) || (this.GetComponent<Transform>().eulerAngles.y == -30f))
            this.GetComponentInParent<AudioSource>().Play();
    }

    public void Interact(ControllerPass pass)
    {
        if (pass.state == ControllerPass.controllerState.Enter)
        {
            trackedController = pass.obj.gameObject.GetComponent<SteamVR_TrackedController>();
            trackedController.Gripped += new ClickedEventHandler(DoGrip);
            //pass.obj.gameObject.GetComponentInChildren<HandModelEditor>().SetModel("Hold");
            ps.Play();
        }
        else if (pass.state == ControllerPass.controllerState.Exit)
        {
            trackedController = pass.obj.gameObject.GetComponent<SteamVR_TrackedController>();
            trackedController.Gripped -= new ClickedEventHandler(DoGrip);
            //pass.obj.gameObject.GetComponentInChildren<HandModelEditor>().SetModel("Neutral");
            ps.Stop();
        }
    }

    void DoGrip(object sender, ClickedEventArgs e)
    {
        this.GetComponent<Rigidbody>().useGravity = false; //Turn off gravity to hold item.
        ps.Stop();

        joints[e.controllerIndex] = this.gameObject.AddComponent<ConfigurableJoint>();
        joints[e.controllerIndex].connectedBody = ((SteamVR_TrackedController)sender).GetComponent<Rigidbody>(); //Define position of joint.
        joints[e.controllerIndex].xMotion = ConfigurableJointMotion.Free;
        joints[e.controllerIndex].yMotion = ConfigurableJointMotion.Free;
        joints[e.controllerIndex].zMotion = ConfigurableJointMotion.Limited;
        joints[e.controllerIndex].angularXMotion = ConfigurableJointMotion.Free;
        joints[e.controllerIndex].angularYMotion = ConfigurableJointMotion.Free;
        joints[e.controllerIndex].angularZMotion = ConfigurableJointMotion.Free;
        joints[e.controllerIndex].linearLimit = softJointLimit;
        joints[e.controllerIndex].linearLimitSpring = softJointLimitSpring;
        joints[e.controllerIndex].projectionMode = JointProjectionMode.None;
        joints[e.controllerIndex].anchor = ((SteamVR_TrackedController)sender).gameObject.transform.position - this.transform.position;
        joints[e.controllerIndex].enablePreprocessing = false;

        ((SteamVR_TrackedController)sender).Ungripped += new ClickedEventHandler(DoUngrip);
    }

    void DoUngrip(object sender, ClickedEventArgs e)
    {
        ((SteamVR_TrackedController)sender).Ungripped -= new ClickedEventHandler(DoUngrip);
        GameObject senderGO = ((SteamVR_TrackedController)sender).gameObject;
        Object.DestroyImmediate(joints[e.controllerIndex]);

        joints[e.controllerIndex] = null;

        bool gripped = false;

        for (int i = 0; i < joints.Length; i++)
            if (joints[i] != null)
                gripped = true;

        if (!gripped)
        {
            Rigidbody rigidbody = this.GetComponent<Rigidbody>();
            rigidbody.GetComponent<Rigidbody>().useGravity = true;

            trackedObj = senderGO.GetComponent<SteamVR_TrackedObject>();
            SteamVR_Controller.Device device = SteamVR_Controller.Input((int)e.controllerIndex);
            Transform origin = trackedObj.origin ? trackedObj.origin : trackedObj.transform.parent;

            if (origin != null)
            {
                rigidbody.velocity = origin.TransformVector(device.velocity);
                rigidbody.angularVelocity = origin.TransformVector(device.angularVelocity);
            }
            else
            {
                rigidbody.velocity = device.velocity;
                rigidbody.angularVelocity = device.angularVelocity;
            }

            rigidbody.maxAngularVelocity = rigidbody.angularVelocity.magnitude;
        }
    }

    void OnJointBreak(float breakForce)
    {
        Debug.LogError("A joint has just been broken!, force: " + breakForce);
    }
}