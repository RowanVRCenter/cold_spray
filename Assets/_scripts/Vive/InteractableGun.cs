﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InteractableGun : MonoBehaviour
{
    SteamVR_TrackedController trackedController;
    SteamVR_TrackedObject trackedObj;
    ConfigurableJoint[] joints = new ConfigurableJoint[15];

    SoftJointLimit softJointLimit = new SoftJointLimit();
    SoftJointLimitSpring softJointLimitSpring = new SoftJointLimitSpring();

    private bool holding;
    private GameObject cModel;

    [SerializeField]
    private ParticleSystem spray;

    [SerializeField]
    private ParticleSystem handle;

    void Awake()
    {
        softJointLimit.bounciness = 0;
        softJointLimit.contactDistance = 0.05f;
        softJointLimit.limit = 0;
        softJointLimitSpring.damper = 100;
        softJointLimitSpring.spring = 100;
        holding = false;
    }

    public void Interact(ControllerPass pass)
    {
        if (pass.state == ControllerPass.controllerState.Enter)
        {
            cModel = pass.model;
            trackedController = pass.obj.gameObject.GetComponent<SteamVR_TrackedController>();
            trackedController.Gripped += new ClickedEventHandler(DoGrip);
            //pass.obj.gameObject.GetComponentInChildren<HandModelEditor>().SetModel("Hold");
            handle.Play();
        }
        else if (pass.state == ControllerPass.controllerState.Exit)
        {
            cModel = null;
            trackedController = pass.obj.gameObject.GetComponent<SteamVR_TrackedController>();
            trackedController.Gripped -= new ClickedEventHandler(DoGrip);
            //pass.obj.gameObject.GetComponentInChildren<HandModelEditor>().SetModel("Neutral");
            handle.Stop();
        }
    }

    void DoGrip(object sender, ClickedEventArgs e)
    {
        handle.Stop();
        this.GetComponent<Rigidbody>().useGravity = false; //Turn off gravity to hold item.

        //Temporary fix to keep the gun from sliding when simulation first starts.
        this.GetComponent<Rigidbody>().drag = 0f; 
        this.GetComponent<Rigidbody>().angularDrag = 0f;

        joints[e.controllerIndex] = this.gameObject.AddComponent<ConfigurableJoint>();
        joints[e.controllerIndex].connectedBody = ((SteamVR_TrackedController)sender).GetComponent<Rigidbody>(); //Define position of joint.
        joints[e.controllerIndex].xMotion = ConfigurableJointMotion.Limited;
        joints[e.controllerIndex].yMotion = ConfigurableJointMotion.Limited;
        joints[e.controllerIndex].zMotion = ConfigurableJointMotion.Limited;
        joints[e.controllerIndex].angularXMotion = ConfigurableJointMotion.Locked;
        joints[e.controllerIndex].angularYMotion = ConfigurableJointMotion.Locked;
        joints[e.controllerIndex].angularZMotion = ConfigurableJointMotion.Locked; 
        joints[e.controllerIndex].linearLimit = softJointLimit;
        joints[e.controllerIndex].linearLimitSpring = softJointLimitSpring;
        joints[e.controllerIndex].projectionMode = JointProjectionMode.None;
        joints[e.controllerIndex].anchor = ((SteamVR_TrackedController)sender).gameObject.transform.position - this.transform.position;
        joints[e.controllerIndex].enablePreprocessing = false;

        ((SteamVR_TrackedController)sender).Ungripped += new ClickedEventHandler(DoUngrip);
        ((SteamVR_TrackedController)sender).PadClicked += new ClickedEventHandler(DoPadDown); //Add listener for pressing the pad.
    }

    void DoTriggerDown(object sender, ClickedEventArgs e)
    {
        ((SteamVR_TrackedController)sender).TriggerUnclicked += new ClickedEventHandler(DoTriggerUp);

        if(((SteamVR_TrackedController)sender).gripped)
            spray.Play();
    }

    void DoTriggerUp(object sender, ClickedEventArgs e)
    {
        spray.Stop();
        ((SteamVR_TrackedController)sender).TriggerUnclicked -= new ClickedEventHandler(DoTriggerUp);
    }

    //Should probably do something to make the orientation correct regardless of how it is picked up.
    void DoPadDown(object sender, ClickedEventArgs e)
    {
        if (!holding) //Press pad to hold gun.
        {
            cModel.SetActive(false); //Hide the hand.

            //Free rotation temporarily.
            joints[e.controllerIndex].angularXMotion = ConfigurableJointMotion.Free;
            joints[e.controllerIndex].angularYMotion = ConfigurableJointMotion.Free;
            joints[e.controllerIndex].angularZMotion = ConfigurableJointMotion.Free;

        //Check this.
            //Orient gun.
            //gameObject.GetComponent<Transform>().rotation.Set(trackedController.GetComponent<Transform>().rotation.x, 
              //                                                trackedController.GetComponent<Transform>().rotation.y, 
                //                                              trackedController.GetComponent<Transform>().rotation.z,
                  //                                            trackedController.GetComponent<Transform>().rotation.w);

            //Lock rotation.
            joints[e.controllerIndex].angularXMotion = ConfigurableJointMotion.Locked;
            joints[e.controllerIndex].angularYMotion = ConfigurableJointMotion.Locked;
            joints[e.controllerIndex].angularZMotion = ConfigurableJointMotion.Locked;

            //Lock x, y, z.
            joints[e.controllerIndex].xMotion = ConfigurableJointMotion.Locked;
            joints[e.controllerIndex].yMotion = ConfigurableJointMotion.Locked;
            joints[e.controllerIndex].zMotion = ConfigurableJointMotion.Locked;

            ((SteamVR_TrackedController)sender).Gripped -= new ClickedEventHandler(DoGrip); //Remove the grip listener while the gun is held.
            ((SteamVR_TrackedController)sender).Ungripped -= new ClickedEventHandler(DoUngrip); //Remove the ungrip listener while the gun is held.

            ((SteamVR_TrackedController)sender).TriggerClicked += new ClickedEventHandler(DoTriggerDown); //Add trigger listener.
        }
        else //Press pad again to let go of the gun.
        {
            cModel.SetActive(true); //Show the hand.

            ((SteamVR_TrackedController)sender).Ungripped += new ClickedEventHandler(DoUngrip); //Add the ungrip listener back.
            ((SteamVR_TrackedController)sender).TriggerClicked -= new ClickedEventHandler(DoTriggerDown); //Remove trigger listener.
        }

        holding = !holding;
    }
    
    void DoUngrip(object sender, ClickedEventArgs e)
    {
        if (!holding)
        {
            ((SteamVR_TrackedController)sender).Ungripped -= new ClickedEventHandler(DoUngrip); //Remove listener for if the grip is released.
            ((SteamVR_TrackedController)sender).PadClicked -= new ClickedEventHandler(DoPadDown); //Remove listener for if the pad is pressed.

            GameObject senderGO = ((SteamVR_TrackedController)sender).gameObject;
            Object.DestroyImmediate(joints[e.controllerIndex]);

            joints[e.controllerIndex] = null;

            bool gripped = false;

            for (int i = 0; i < joints.Length; i++)
                if (joints[i] != null)
                    gripped = true;

            if (!gripped)
            {
                Rigidbody rigidbody = this.GetComponent<Rigidbody>();
                rigidbody.GetComponent<Rigidbody>().useGravity = true;

                trackedObj = senderGO.GetComponent<SteamVR_TrackedObject>();
                SteamVR_Controller.Device device = SteamVR_Controller.Input((int)e.controllerIndex);

                Transform origin = trackedObj.origin ? trackedObj.origin : trackedObj.transform.parent;

                if (origin != null)
                {
                    rigidbody.velocity = origin.TransformVector(device.velocity);
                    rigidbody.angularVelocity = origin.TransformVector(device.angularVelocity);
                }
                else
                {
                    rigidbody.velocity = device.velocity;
                    rigidbody.angularVelocity = device.angularVelocity;
                }

                rigidbody.maxAngularVelocity = rigidbody.angularVelocity.magnitude;
            }
        }
    }

    void OnJointBreak(float breakForce)
    {
        Debug.LogError("A joint has just been broken!, force: " + breakForce);
    }
}