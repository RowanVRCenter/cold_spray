﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InteractablePowderBottle : MonoBehaviour
{
    SteamVR_TrackedController trackedController;
    SteamVR_TrackedObject trackedObj;
    ConfigurableJoint[] joints = new ConfigurableJoint[15];

    SoftJointLimit softJointLimit = new SoftJointLimit();
    SoftJointLimitSpring softJointLimitSpring = new SoftJointLimitSpring();

    private bool capped;
    private bool inFeeder;

    [SerializeField]
    private GameObject cap;

    [SerializeField]
    private GameObject bottlePowder;

    [SerializeField]
    private GameObject feederPowder;

    void Awake()
    {
        softJointLimit.bounciness = 0;
        softJointLimit.contactDistance = 0.05f;
        softJointLimit.limit = 0;
        softJointLimitSpring.damper = 100;
        softJointLimitSpring.spring = 100;
        capped = true;
        inFeeder = false;
    }

    void Update()
    {
        if ((!capped) && (bottlePowder.gameObject.activeSelf) && (Mathf.Abs(gameObject.transform.rotation.z) >= 90f))
        {
            bottlePowder.transform.localScale.Set(1f, (bottlePowder.transform.localScale.y * .9f), 1f);

            if (inFeeder)
                feederPowder.transform.localScale.Set(1f, 1f, feederPowder.transform.localScale.y + 0.2f);
        }

        if (bottlePowder.transform.localScale.y <= .1f)
            bottlePowder.gameObject.SetActive(false);
    }

    public void Interact(ControllerPass pass)
    {
        if (pass.state == ControllerPass.controllerState.Enter)
        {
            trackedController = pass.obj.gameObject.GetComponent<SteamVR_TrackedController>();
            trackedController.Gripped += new ClickedEventHandler(DoGrip);
            //pass.obj.gameObject.GetComponentInChildren<HandModelEditor>().SetModel("Hold");
            GetComponent<ParticleSystem>().Play();
        }
        else if (pass.state == ControllerPass.controllerState.Exit)
        {
            trackedController = pass.obj.gameObject.GetComponent<SteamVR_TrackedController>();
            trackedController.Gripped -= new ClickedEventHandler(DoGrip);
            //pass.obj.gameObject.GetComponentInChildren<HandModelEditor>().SetModel("Neutral");
            GetComponent<ParticleSystem>().Stop();
        }
    }

    void DoGrip(object sender, ClickedEventArgs e)
    {
        GetComponent<ParticleSystem>().Stop();
        GetComponent<Rigidbody>().useGravity = false; //Turn off gravity to hold item.

        //((GameObject)sender).gameObject.GetComponentInChildren<HandModelEditor>().SetModel("Hold");

        joints[e.controllerIndex] = gameObject.AddComponent<ConfigurableJoint>();
        joints[e.controllerIndex].connectedBody = ((SteamVR_TrackedController)sender).GetComponent<Rigidbody>(); //Define position of joint.
        joints[e.controllerIndex].xMotion = ConfigurableJointMotion.Limited;
        joints[e.controllerIndex].yMotion = ConfigurableJointMotion.Limited;
        joints[e.controllerIndex].zMotion = ConfigurableJointMotion.Limited;
        joints[e.controllerIndex].angularXMotion = ConfigurableJointMotion.Locked;
        joints[e.controllerIndex].angularYMotion = ConfigurableJointMotion.Locked;
        joints[e.controllerIndex].angularZMotion = ConfigurableJointMotion.Locked;
        joints[e.controllerIndex].linearLimit = softJointLimit;
        joints[e.controllerIndex].linearLimitSpring = softJointLimitSpring;
        joints[e.controllerIndex].projectionMode = JointProjectionMode.None;
        joints[e.controllerIndex].anchor = ((SteamVR_TrackedController)sender).gameObject.transform.position - transform.position;
        joints[e.controllerIndex].enablePreprocessing = false;

        ((SteamVR_TrackedController)sender).Ungripped += new ClickedEventHandler(DoUngrip);
        ((SteamVR_TrackedController)sender).PadClicked += new ClickedEventHandler(DoPadDown);

        feederPowder.gameObject.GetComponent<MeshRenderer>().materials[0] = bottlePowder.gameObject.GetComponent<MeshRenderer>().materials[0];
    }

    void DoUngrip(object sender, ClickedEventArgs e)
    {
        GetComponent<ParticleSystem>().Stop();

        //((GameObject)sender).gameObject.GetComponentInChildren<HandModelEditor>().SetModel("Neutral");

        ((SteamVR_TrackedController)sender).Gripped -= new ClickedEventHandler(DoGrip);
        ((SteamVR_TrackedController)sender).Ungripped -= new ClickedEventHandler(DoUngrip);
        ((SteamVR_TrackedController)sender).PadClicked -= new ClickedEventHandler(DoPadDown);

        GameObject senderGO = ((SteamVR_TrackedController)sender).gameObject;
        Object.DestroyImmediate(joints[e.controllerIndex]);

        joints[e.controllerIndex] = null;

        bool gripped = false;

        for (int i = 0; i < joints.Length; i++)
            if (joints[i] != null)
                gripped = true;

        if (!gripped)
        {
            Rigidbody rigidbody = GetComponent<Rigidbody>();
            rigidbody.GetComponent<Rigidbody>().useGravity = true;

            trackedObj = senderGO.GetComponent<SteamVR_TrackedObject>();
            SteamVR_Controller.Device device = SteamVR_Controller.Input((int)e.controllerIndex);

            Transform origin = trackedObj.origin ? trackedObj.origin : trackedObj.transform.parent;

            if (origin != null)
            {
                rigidbody.velocity = origin.TransformVector(device.velocity);
                rigidbody.angularVelocity = origin.TransformVector(device.angularVelocity);
            }
            else
            {
                rigidbody.velocity = device.velocity;
                rigidbody.angularVelocity = device.angularVelocity;
            }

            rigidbody.maxAngularVelocity = rigidbody.angularVelocity.magnitude;
        }
    }

    void DoPadDown(object sender, ClickedEventArgs e)
    {
        if (capped)
            cap.SetActive(false);
        else
            cap.SetActive(true);

        capped = !capped;
    }

    void OnJointBreak(float breakForce)
    {
        Debug.LogError("A joint has just been broken!, force: " + breakForce);
    }

    void OnColliderEnter(Collider coll)
    {
        //Using a switch in case we want to add other special cases.
        switch (coll.gameObject.name)
        {
            case "HopperCollider":
                inFeeder = true;
                break;
        }
    }
}