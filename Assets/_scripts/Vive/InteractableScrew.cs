﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InteractableScrew : MonoBehaviour
{
    SteamVR_TrackedController trackedController;
    SteamVR_TrackedObject trackedObj;
    ConfigurableJoint[] joints = new ConfigurableJoint[15];

    SoftJointLimit softJointLimit = new SoftJointLimit();
    SoftJointLimitSpring softJointLimitSpring = new SoftJointLimitSpring();

    private bool used;

    [SerializeField]
    private GameObject cap;

    void Awake()
    {
        softJointLimit.bounciness = 0;
        softJointLimit.contactDistance = 0.05f;
        softJointLimit.limit = 0;
        softJointLimitSpring.damper = 100;
        softJointLimitSpring.spring = 100;
        used = false;
    }

    public void Interact(ControllerPass pass)
    {
        if (pass.state == ControllerPass.controllerState.Enter)
        {
            trackedController = pass.obj.gameObject.GetComponent<SteamVR_TrackedController>();
            trackedController.Gripped += new ClickedEventHandler(DoGrip);
            //pass.obj.gameObject.GetComponentInChildren<HandModelEditor>().SetModel("Fist");
            this.GetComponent<ParticleSystem>().Play();
        }
        else if (pass.state == ControllerPass.controllerState.Exit)
        {
            trackedController = pass.obj.gameObject.GetComponent<SteamVR_TrackedController>();
            trackedController.Gripped -= new ClickedEventHandler(DoGrip);
            //pass.obj.gameObject.GetComponentInChildren<HandModelEditor>().SetModel("Neutral");
            this.GetComponent<ParticleSystem>().Stop();
        }
    }

    void DoGrip(object sender, ClickedEventArgs e)
    {
        //Froze position to start with, need to now unfreeze.
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        this.GetComponent<ParticleSystem>().Stop();
        this.GetComponent<Rigidbody>().useGravity = false; //Turn off gravity to hold item.

        joints[e.controllerIndex] = this.gameObject.AddComponent<ConfigurableJoint>();
        joints[e.controllerIndex].connectedBody = ((SteamVR_TrackedController)sender).GetComponent<Rigidbody>(); //Define position of joint.
        joints[e.controllerIndex].xMotion = ConfigurableJointMotion.Locked;
        joints[e.controllerIndex].yMotion = ConfigurableJointMotion.Locked;
        joints[e.controllerIndex].zMotion = ConfigurableJointMotion.Locked;
        joints[e.controllerIndex].angularXMotion = ConfigurableJointMotion.Locked;
        joints[e.controllerIndex].angularYMotion = ConfigurableJointMotion.Locked;
        joints[e.controllerIndex].angularZMotion = ConfigurableJointMotion.Locked;
        joints[e.controllerIndex].linearLimit = softJointLimit;
        joints[e.controllerIndex].linearLimitSpring = softJointLimitSpring;
        joints[e.controllerIndex].projectionMode = JointProjectionMode.None;
        joints[e.controllerIndex].anchor = ((SteamVR_TrackedController)sender).gameObject.transform.position - this.transform.position;
        joints[e.controllerIndex].enablePreprocessing = false;

        ((SteamVR_TrackedController)sender).Ungripped += new ClickedEventHandler(DoUngrip);
    }

    void DoUngrip(object sender, ClickedEventArgs e)
    {
        ((SteamVR_TrackedController)sender).Ungripped -= new ClickedEventHandler(DoUngrip);
        GameObject senderGO = ((SteamVR_TrackedController)sender).gameObject;
        Object.DestroyImmediate(joints[e.controllerIndex]);

        joints[e.controllerIndex] = null;

        bool gripped = false;

        for (int i = 0; i < joints.Length; i++)
            if (joints[i] != null)
                gripped = true;

        if (!gripped)
        {
            Rigidbody rigidbody = this.GetComponent<Rigidbody>();
            rigidbody.GetComponent<Rigidbody>().useGravity = true;

            trackedObj = senderGO.GetComponent<SteamVR_TrackedObject>();
            SteamVR_Controller.Device device = SteamVR_Controller.Input((int)e.controllerIndex);
            Transform origin = trackedObj.origin ? trackedObj.origin : trackedObj.transform.parent;

            if (origin != null)
            {
                rigidbody.velocity = origin.TransformVector(device.velocity);
                rigidbody.angularVelocity = origin.TransformVector(device.angularVelocity);
            }
            else
            {
                rigidbody.velocity = device.velocity;
                rigidbody.angularVelocity = device.angularVelocity;
            }

            rigidbody.maxAngularVelocity = rigidbody.angularVelocity.magnitude;
        }

        if(!used)
            cap.SendMessage("RemoveScrew", SendMessageOptions.DontRequireReceiver);

        this.used = true;
    }

    void OnJointBreak(float breakForce)
    {
        Debug.LogError("A joint has just been broken!, force: " + breakForce);
    }
}