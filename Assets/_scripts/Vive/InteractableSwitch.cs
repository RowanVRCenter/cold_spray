﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InteractableSwitch : MonoBehaviour
{
    SteamVR_TrackedController trackedController;
    SteamVR_TrackedObject trackedObj;

    private bool on = false;

    public void Interact(ControllerPass pass)
    {
        if (pass.state == ControllerPass.controllerState.Enter)
        {
            trackedController = pass.obj.gameObject.GetComponent<SteamVR_TrackedController>();
            trackedController.PadClicked += new ClickedEventHandler(DoPad);
            //pass.obj.gameObject.GetComponentInChildren<HandModelEditor>().SetModel("Point");
            GetComponent<ParticleSystem>().Play();
        }
        else if (pass.state == ControllerPass.controllerState.Exit)
        {
            trackedController = pass.obj.gameObject.GetComponent<SteamVR_TrackedController>();
            trackedController.PadClicked -= new ClickedEventHandler(DoPad);
            //pass.obj.gameObject.GetComponentInChildren<HandModelEditor>().SetModel("Netural");
            GetComponent<ParticleSystem>().Stop();
        }
    }

    void DoPad(object sender, ClickedEventArgs e)
    {
        GetComponent<ParticleSystem>().Stop();

        if (!on)
            GetComponent<Transform>().Rotate(0f, -20f, 0f);
        else
            GetComponent<Transform>().Rotate(0f, 20f, 0f);

        on = !on;
    }
}