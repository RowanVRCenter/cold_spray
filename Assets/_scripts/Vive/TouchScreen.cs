﻿using UnityEngine;
using System.Collections;

public class TouchScreen : MonoBehaviour
{
    protected GameObject previousMenu;
    protected GameObject currentMenu;

    [SerializeField]
    private GameObject[] menus;
    //0 = Start, 1 = Auto, 2 = Manual, 3 = ExtManual, 4 = IO, 5 = MachParam, 6 = OpSettings, 7 = SysPreheat

    void Awake()
    {
        currentMenu = menus[0];
    }

    private void OnCollision(Collider coll)
    {
        //gameObject.GetComponent<HandModelEditor>().SetModel("Point");

        switch (coll.gameObject.name)
        {
            //For Startup Buttons  
            case "VRC_StartupScreen_Button_Manual":
                goToMenu(menus[2]);
                break;
            case "VRC_StartupScreen_Button_Auto":
                goToMenu(menus[1]);
                break;

            //For Auto Buttons
            case "VRC_Auto_BottomMenu_StartUpScreen_00":
                goToMenu(menus[0]);
                break;
            case "VRC_Auto_BottomMenu_OperatorInfo_01":
                break;
            case "VRC_Auto_BottomMenu_Recipes_02":
                break;
            case "VRC_Auto_BottomMenu_ExtManual_03":
                goToMenu(menus[3]);
                break;
            case "VRC_Auto_BottomMenu_Manual_04":
                goToMenu(menus[2]);
                break;
            case "VRC_Auto_BottomMenu_SystemPreheat_05":
                goToMenu(menus[7]);
                break;
            case "VRC_Auto_BottomMenu_OperatorSettings_06":
                goToMenu(menus[6]);
                break;
            case "VRC_Auto_BottomMenu_Back_07":
                goBack();
                break;
            case "VRC_Auto_Button_Auto":
                break;
            case "VRC_Auto_Button_Off":
                break;
            case "VRC_Auto_Button_DisableAll":
                break;

            //For Manual Buttons
            case "VRC_Manual_BottomMenu_StartUpScreen_00":
                goToMenu(menus[0]);
                break;
            case "VRC_Manual_BottomMenu_SystemPreheat_01":
                goToMenu(menus[7]);
                break;
            case "VRC_Manual_BottomMenu_ExtManual_02":
                goToMenu(menus[3]);
                break;
            case "VRC_Manual_BottomMenu_Auto_03":
                goToMenu(menus[1]);
                break;
            case "VRC_Manual_BottomMenu_Recipes_04":
                break;
            case "VRC_Manual_BottomMenu_OperatorInfo_05":
                break;
            case "VRC_Manual_BottomMenu_OperatorSettings_06":
                goToMenu(menus[6]);
                break;
            case "VRC_Manual_BottomMenu_Back_07":
                goBack();
                break;

            //For ExtManual
            case "VRC_ExtManual_BottomMenu_SystemPreheat_00":
                goToMenu(menus[7]);
                break;
            case "VRC_ExtManual_BottomMenu_MachineParameters_01":
                goToMenu(menus[5]);
                break;
            case "VRC_ExtManual_BottomMenu_SystemScreen_02":
                break;
            case "VRC_ExtManual_BottomMenu_IOStatus_03":
                goToMenu(menus[4]);
                break;
            case "VRC_ExtManual_BottomMenu_Manual_04":
                goToMenu(menus[2]);
                break;
            case "VRC_ExtManual_BottomMenu_Auto_05":
                goToMenu(menus[1]);
                break;
            case "VRC_ExtManual_BottomMenu_OperatorSettings_06":
                goToMenu(menus[6]);
                break;
            case "VRC_ExtManual_BottomMenu_Back_07":
                goBack();
                break;

            //IOStatus
            case "VRC_IOStatus_BottomMenu_StartUpScreen_00":
                goToMenu(menus[0]);
                break;
            case "VRC_IOStatus_BottomMenu_ExtManual_01":
                goToMenu(menus[3]);
                break;
            case "VRC_IOStatus_BottomMenu_Auto_02":
                goToMenu(menus[1]);
                break;
            case "VRC_IOStatus_BottomMenu_OperatorSettings_06":
                goToMenu(menus[6]);
                break;
            case "VRC_IOStatus_BottomMenu_Back_07":
                goBack();
                break;

            //Machine Parameters
            case "VRC_MachineParameters_BottomMenu_StartUpScreen_00":
                goToMenu(menus[0]);
                break;
            case "VRC_MachineParameters_BottomMenu_CalibrationScreen_01":
                break;
            case "VRC_MachineParameters_BottomMenu_ExtManual_02":
                goToMenu(menus[3]);
                break;
            case "VRC_MachineParameters_BottomMenu_Back_07":
                goBack();
                break;

            //Operator Settings
            case "VRC_OperatorSettings_BottomMenu_StartUpScreen_00":
                goToMenu(menus[0]);
                break;
            case "VRC_OperatorSettings_BottomMenu_IOStatus_01":
                goToMenu(menus[4]);
                break;
            case "VRC_OperatorSettings_BottomMenu_Manual_02":
                goToMenu(menus[2]);
                break;
            case "VRC_OperatorSettings_BottomMenu_Auto_03":
                goToMenu(menus[1]);
                break;
            case "VRC_OperatorSettings_BottomMenu_Back_07":
                goBack();
                break;

            //System Preheat
            case "VRC_SystemPreheat_BottomMenu_StartUpScreen_00":
                goToMenu(menus[0]);
                break;
            case "VRC_SystemPreheat_BottomMenu_IOStatus_01":
                goToMenu(menus[4]);
                break;
            case "VRC_SystemPreheat_BottomMenu_Manual_02":
                goToMenu(menus[2]);
                break;
            case "VRC_SystemPreheat_BottomMenu_Auto_03":
                goToMenu(menus[1]);
                break;
            case "VRC_SystemPreheat_BottomMenu_Recipes_04":
                break;
            case "VRC_SystemPreheat_BottomMenu_OperatorInfo_05":
                break;
            case "VRC_SystemPreheat_BottomMenu_OperatorSettings_06":
                goToMenu(menus[6]);
                break;
            case "VRC_SystemPreheat_BottomMenu_Back_07":
                goBack();
                break;
        }
    }

    protected void goToMenu(GameObject menu)
    {
        currentMenu.SetActive(false);
        previousMenu = currentMenu;
        currentMenu = menu;
        currentMenu.SetActive(true);
    }

    protected void goBack()
    {
        currentMenu.SetActive(false);
        GameObject temp = currentMenu;
        currentMenu = previousMenu;
        previousMenu = temp;
        currentMenu.SetActive(true);
    }
}