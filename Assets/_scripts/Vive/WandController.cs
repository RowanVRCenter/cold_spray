﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SteamVR_TrackedObject))]
[RequireComponent(typeof(SteamVR_TrackedController))]
public class WandController : MonoBehaviour
{
    [SerializeField]
    private GameObject controllerModel;

    private SteamVR_TrackedObject trackedObj;
    private GameObject pickup;

    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
    }

    private void OnTriggerEnter(Collider collider) 
    {
        ControllerPass pass = new ControllerPass();
        pass.obj = trackedObj;
        pass.state = ControllerPass.controllerState.Enter;
        pass.model = controllerModel;
        collider.gameObject.SendMessage("Interact", pass, SendMessageOptions.DontRequireReceiver);
    }

    private void OnTriggerExit(Collider collider)
    {

        ControllerPass pass = new ControllerPass();
        pass.obj = trackedObj;
        pass.state = ControllerPass.controllerState.Exit;
        collider.gameObject.SendMessage("Interact", pass, SendMessageOptions.DontRequireReceiver);
    }
 }